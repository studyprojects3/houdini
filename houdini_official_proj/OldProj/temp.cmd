# Automatically generated script
\set noalias = 1
set saved_path = `execute("oppwf")`
opcf /

# Node out (/out)
opcf out

# Node flowmap_ssrpg_flowmap (labs::Driver/games_baker::2.0)
opadd -e -n labs::games_baker::2.0 flowmap_ssrpg_flowmap
oplocate -x -2.98691 -y -0.077525000000000011 flowmap_ssrpg_flowmap
opspareds "" flowmap_ssrpg_flowmap
opparm flowmap_ssrpg_flowmap  meshes ( 1 ) custom_channels ( 0 )
chblockbegin
chadd -t 0 0 flowmap_ssrpg_flowmap currentplane
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'hou.pwd().hdaModule().setplane(hou.pwd())' -l p flowmap_ssrpg_flowmap/currentplane
chadd -t 0 0 flowmap_ssrpg_flowmap f1
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$RFSTART' flowmap_ssrpg_flowmap/f1
chadd -t 0 0 flowmap_ssrpg_flowmap f2
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$RFEND' flowmap_ssrpg_flowmap/f2
chblockend
opparm -V 657 flowmap_ssrpg_flowmap execute ( 0 ) renderdialog ( 0 ) meshes ( 1 ) vm_uvunwrapres ( 2048 2048 ) uvresmenu ( '256 256' ) base_path ( '$HIP/render/${HIPNAME}_flowmap_bg.png' ) tempdirectory ( '$TEMP/houdini_temp' ) privateparmsfolder ( 1 1 1 ) overridenode ( '`opfullpath(".")' ) initswitch ( on ) uuid ( 1dab8bd7-5d9f-4a3e-a761-65d70e693e2a ) current_low_mesh ( /obj/bg/OUT_Flowmap ) current_high_mesh ( /obj/bg/OUT_Flowmap ) tempexrpath ( 'C:\\Users\\xd\\AppData\\Local\\Temp/houdini_temp/1dab8bd7-5d9f-4a3e-a761-65d70e693e2a_tmp_exr_path1.exr' ) tempcoppath ( '`chs("tempdirectory")`/`chs("uuid")`_tmp_cop_path.exr' ) udimpostprocess ( diffusefill ) currentcoppath ( ../extract_net/base_image ) currentcopoutput ( '`chs("tempdirectory")`/initcop.tga' ) currentplane ( currentplane ) currentgamma ( 2.2000000000000002 ) currentcustomattr ( v ) currentcustomsuffix ( velocity ) bake_basecolor ( on ) basecolor_suffix ( basecolor ) bake_Nt ( off ) Nt_suffix ( normal ) bake_alpha ( off ) alpha_suffix ( alpha ) bake_specrough ( off ) specrough_suffix ( roughness ) bake_metallic ( off ) metallic_suffix ( metallic ) bake_N ( off ) N_suffix ( worldnormal ) bake_Oc ( off ) Oc_suffix ( ao ) bake_Cu ( off ) Cu_suffix ( curvature ) bake_Th ( off ) Th_suffix ( thickness ) bake_P ( off ) P_suffix ( position ) bake_Ds ( off ) Ds_suffix ( height ) custom_channels ( 0 ) trange ( off ) f ( f1 f2 1 ) vm_bake_usemikkt ( on ) bBasecolorLinearSpace ( on ) vm_bake_tangentnormalflipy ( off ) vm_bake_skipcf ( on ) vm_uvcageobject1 ( "" ) border_padding ( 2 ) baking_samples ( 8 ) ray_bias ( 0.10000000000000001 ) ray_distance ( -1 ) target_mesh_1 ( /obj/bg/OUT_Flowmap ) source_mesh_1 ( /obj/bg/OUT_Flowmap )
chlock flowmap_ssrpg_flowmap -*
chautoscope flowmap_ssrpg_flowmap -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off flowmap_ssrpg_flowmap
opexprlanguage -s hscript flowmap_ssrpg_flowmap
opuserdata -n '___Version___' -v '657' flowmap_ssrpg_flowmap
opuserdata -n 'wirestyle' -v 'rounded' flowmap_ssrpg_flowmap

# Node flowmap_ssrpg_river_mask (labs::Driver/games_baker::2.0)
opadd -e -n labs::games_baker::2.0 flowmap_ssrpg_river_mask
oplocate -x -2.98691 -y -1.0306474999999997 flowmap_ssrpg_river_mask
opspareds "" flowmap_ssrpg_river_mask
opparm flowmap_ssrpg_river_mask  meshes ( 1 ) custom_channels ( 0 )
chblockbegin
chadd -t 0 0 flowmap_ssrpg_river_mask currentplane
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'hou.pwd().hdaModule().setplane(hou.pwd())' -l p flowmap_ssrpg_river_mask/currentplane
chadd -t 0 0 flowmap_ssrpg_river_mask f1
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$RFSTART' flowmap_ssrpg_river_mask/f1
chadd -t 0 0 flowmap_ssrpg_river_mask f2
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$RFEND' flowmap_ssrpg_river_mask/f2
chblockend
opparm -V 657 flowmap_ssrpg_river_mask execute ( 0 ) renderdialog ( 0 ) meshes ( 1 ) vm_uvunwrapres ( 2048 2048 ) uvresmenu ( '256 256' ) base_path ( '$HIP/render/${HIPNAME}_river_mask.png' ) tempdirectory ( '$TEMP/houdini_temp' ) privateparmsfolder ( 1 1 1 ) overridenode ( '`opfullpath(".")' ) initswitch ( on ) uuid ( ddbc788a-f31e-4fa3-8ca9-30863f0f5241 ) current_low_mesh ( /obj/bg/OUT_RiverMask ) current_high_mesh ( /obj/bg/OUT_RiverMask ) tempexrpath ( 'C:\\Users\\xd\\AppData\\Local\\Temp/houdini_temp/ddbc788a-f31e-4fa3-8ca9-30863f0f5241_tmp_exr_path1.exr' ) tempcoppath ( '`chs("tempdirectory")`/`chs("uuid")`_tmp_cop_path.exr' ) udimpostprocess ( diffusefill ) currentcoppath ( ../extract_net/base_image ) currentcopoutput ( '`chs("tempdirectory")`/initcop.tga' ) currentplane ( currentplane ) currentgamma ( 2.2000000000000002 ) currentcustomattr ( v ) currentcustomsuffix ( velocity ) bake_basecolor ( on ) basecolor_suffix ( basecolor ) bake_Nt ( off ) Nt_suffix ( normal ) bake_alpha ( off ) alpha_suffix ( alpha ) bake_specrough ( off ) specrough_suffix ( roughness ) bake_metallic ( off ) metallic_suffix ( metallic ) bake_N ( off ) N_suffix ( worldnormal ) bake_Oc ( off ) Oc_suffix ( ao ) bake_Cu ( off ) Cu_suffix ( curvature ) bake_Th ( off ) Th_suffix ( thickness ) bake_P ( off ) P_suffix ( position ) bake_Ds ( off ) Ds_suffix ( height ) custom_channels ( 0 ) trange ( off ) f ( f1 f2 1 ) vm_bake_usemikkt ( on ) bBasecolorLinearSpace ( on ) vm_bake_tangentnormalflipy ( off ) vm_bake_skipcf ( on ) vm_uvcageobject1 ( "" ) border_padding ( 2 ) baking_samples ( 8 ) ray_bias ( 0.10000000000000001 ) ray_distance ( -1 ) target_mesh_1 ( /obj/bg/OUT_RiverMask ) source_mesh_1 ( /obj/bg/OUT_RiverMask )
chlock flowmap_ssrpg_river_mask -*
chautoscope flowmap_ssrpg_river_mask -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off flowmap_ssrpg_river_mask
opexprlanguage -s hscript flowmap_ssrpg_river_mask
opuserdata -n '___Version___' -v '657' flowmap_ssrpg_river_mask
opuserdata -n 'wirestyle' -v 'rounded' flowmap_ssrpg_river_mask

# Node flowmap_ssrpg_river_mask_EX (labs::Driver/games_baker::2.0)
opadd -e -n labs::games_baker::2.0 flowmap_ssrpg_river_mask_EX
oplocate -x -2.98691 -y -2.0931474999999997 flowmap_ssrpg_river_mask_EX
opspareds "" flowmap_ssrpg_river_mask_EX
opparm flowmap_ssrpg_river_mask_EX  meshes ( 1 ) custom_channels ( 0 )
chblockbegin
chadd -t 0 0 flowmap_ssrpg_river_mask_EX currentplane
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'hou.pwd().hdaModule().setplane(hou.pwd())' -l p flowmap_ssrpg_river_mask_EX/currentplane
chadd -t 0 0 flowmap_ssrpg_river_mask_EX f1
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$RFSTART' flowmap_ssrpg_river_mask_EX/f1
chadd -t 0 0 flowmap_ssrpg_river_mask_EX f2
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$RFEND' flowmap_ssrpg_river_mask_EX/f2
chblockend
opparm -V 657 flowmap_ssrpg_river_mask_EX execute ( 0 ) renderdialog ( 0 ) meshes ( 1 ) vm_uvunwrapres ( 2048 2048 ) uvresmenu ( '256 256' ) base_path ( '$HIP/render/${HIPNAME}_river_mask_ex.png' ) tempdirectory ( '$TEMP/houdini_temp' ) privateparmsfolder ( 1 1 1 ) overridenode ( '`opfullpath(".")' ) initswitch ( on ) uuid ( 28b364f8-0719-44e1-adb4-e1c3f0fe666e ) current_low_mesh ( /obj/bg/OUT_RiverMaskEx ) current_high_mesh ( /obj/bg/OUT_RiverMaskEx ) tempexrpath ( 'C:\\Users\\xd\\AppData\\Local\\Temp/houdini_temp/28b364f8-0719-44e1-adb4-e1c3f0fe666e_tmp_exr_path1.exr' ) tempcoppath ( '`chs("tempdirectory")`/`chs("uuid")`_tmp_cop_path.exr' ) udimpostprocess ( diffusefill ) currentcoppath ( ../extract_net/base_image ) currentcopoutput ( '`chs("tempdirectory")`/initcop.tga' ) currentplane ( currentplane ) currentgamma ( 2.2000000000000002 ) currentcustomattr ( v ) currentcustomsuffix ( velocity ) bake_basecolor ( on ) basecolor_suffix ( basecolor ) bake_Nt ( off ) Nt_suffix ( normal ) bake_alpha ( off ) alpha_suffix ( alpha ) bake_specrough ( off ) specrough_suffix ( roughness ) bake_metallic ( off ) metallic_suffix ( metallic ) bake_N ( off ) N_suffix ( worldnormal ) bake_Oc ( off ) Oc_suffix ( ao ) bake_Cu ( off ) Cu_suffix ( curvature ) bake_Th ( off ) Th_suffix ( thickness ) bake_P ( off ) P_suffix ( position ) bake_Ds ( off ) Ds_suffix ( height ) custom_channels ( 0 ) trange ( off ) f ( f1 f2 1 ) vm_bake_usemikkt ( on ) bBasecolorLinearSpace ( on ) vm_bake_tangentnormalflipy ( off ) vm_bake_skipcf ( on ) vm_uvcageobject1 ( "" ) border_padding ( 2 ) baking_samples ( 8 ) ray_bias ( 0.10000000000000001 ) ray_distance ( -1 ) target_mesh_1 ( /obj/bg/OUT_RiverMaskEx ) source_mesh_1 ( /obj/bg/OUT_RiverMaskEx )
chlock flowmap_ssrpg_river_mask_EX -*
chautoscope flowmap_ssrpg_river_mask_EX -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off flowmap_ssrpg_river_mask_EX
opexprlanguage -s hscript flowmap_ssrpg_river_mask_EX
opuserdata -n '___Version___' -v '657' flowmap_ssrpg_river_mask_EX
opuserdata -n 'wirestyle' -v 'rounded' flowmap_ssrpg_river_mask_EX
opcf ..

# Node vex (/vex)

# Node mat (/mat)
opcf mat

# Node bg_mat (Vop/principledshader::2.0)
opadd -e -n principledshader::2.0 bg_mat
oplocate -x -0.65000000000000002 -y 1.7649999999999999 bg_mat
opspareds "" bg_mat
chblockbegin
chadd -t 0 0 bg_mat specular_tint
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'ch("metallic")+(1-ch("metallic"))*ch("reflecttint")' bg_mat/specular_tint
chadd -t 0 0 bg_mat baseBump_useTexture
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'ch("baseBumpAndNormal_enable") && strcmp(chs("baseBumpAndNormal_type"), "bump") == 0' bg_mat/baseBump_useTexture
chadd -t 0 0 bg_mat baseNormal_useTexture
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'ch("baseBumpAndNormal_enable") && strcmp(chs("baseBumpAndNormal_type"), "normal") == 0' bg_mat/baseNormal_useTexture
chadd -t 0 0 bg_mat shop_disable_displace_shader
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '!ch("dispInput_enable") && !ch("dispTex_enable") && !ch("dispNoise_enable")' bg_mat/shop_disable_displace_shader
chadd -t 0 0 bg_mat vm_displacebound
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '1.01*ch("dispInput_enable")*ch("dispInput_max") + ch("dispTex_enable")*max(abs((1.0+ch("dispTex_offset"))*ch("dispTex_scale")), abs(ch("dispTex_offset")*ch("dispTex_scale"))) + ch("dispNoise_enable")*abs(ch("dispNoise_amp"))' bg_mat/vm_displacebound
chadd -t 0 0 bg_mat speccolorr
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'ch("basecolorr")' bg_mat/speccolorr
chadd -t 0 0 bg_mat speccolorg
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'ch("basecolorg")' bg_mat/speccolorg
chadd -t 0 0 bg_mat speccolorb
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F 'ch("basecolorb")' bg_mat/speccolorb
chblockend
opparm bg_mat specmodel ( ggx ) coatspecmodel ( ggx ) specular_tint ( specular_tint ) diffuse_folder_15 ( 2 2 2 2 2 2 ) folder7 ( 1 ) basecolor ( 1 1 1 ) albedomult ( 1 ) basecolor_usePointColor ( on ) basecolor_usePackedColor ( on ) frontface ( on ) folder4 ( 0 ) ior ( 1.5 ) rough ( 0.29999999999999999 ) aniso ( 0 ) anisodir ( 0 ) folder12 ( 0 ) metallic ( 0 ) reflect ( 1 ) reflecttint ( 0 ) coat ( 0 ) coatrough ( 0 ) folder13 ( 0 ) transparency ( 0 ) transcolor ( 1 1 1 ) transdist ( 0.10000000000000001 ) dispersion ( 0 ) priority ( 0 ) transcolor_usePointColor ( off ) folder8 ( 0 ) sss ( 0 ) ssscolor ( 1 1 1 ) sssmodel ( pbrsss ) sssdist ( 0.10000000000000001 ) sssphase ( 0 ) ssscolor_usePointColor ( off ) folder11 ( 0 ) sheen ( 0 ) sheentint ( 0 ) folder9 ( 0 ) emitint ( 1 ) emitcolor ( 0 0 0 ) emitcolor_usePointColor ( off ) emitillum ( on ) folder15 ( 0 ) opac ( 1 ) opaccolor ( 1 1 1 ) opacpointalpha ( off ) opacpackedalpha ( on ) folder6 ( 0 ) fakecausticsenabled ( off ) fakecausticstransmit ( 1 ) fakecausticsshadow ( 1 ) fakecausticsopacity ( 0 ) folder17 ( 0 ) alphablendmode ( blend ) alphacutoff ( 0.5 ) folder54 ( 0 ) basecolor_useTexture ( on ) basecolor_texture ( '$HIP/flowmaps/bg.tga' ) basecolor_textureIntensity ( 1 ) basecolor_textureWrap ( repeat ) basecolor_textureColorSpace ( auto ) basecolor_useTextureAlpha ( on ) folder1 ( 0 ) ior_useTexture ( off ) ior_texture ( "" ) ior_monoChannel ( 0 ) ior_textureWrap ( repeat ) ior_textureColorSpace ( linear ) folder227 ( 0 ) rough_useTexture ( off ) rough_texture ( "" ) rough_monoChannel ( 0 ) rough_textureWrap ( repeat ) rough_textureColorSpace ( linear ) folder228 ( 0 ) aniso_useTexture ( off ) aniso_texture ( "" ) aniso_monoChannel ( 0 ) aniso_textureWrap ( repeat ) aniso_textureColorSpace ( linear ) folder229 ( 0 ) anisodir_useTexture ( off ) anisodir_texture ( "" ) anisodir_monoChannel ( 0 ) anisodir_textureWrap ( repeat ) anisodir_textureColorSpace ( linear ) anisodir_textureFilter ( point ) folder55 ( 0 ) metallic_useTexture ( off ) metallic_texture ( "" ) metallic_monoChannel ( 0 ) metallic_textureWrap ( repeat ) metallic_textureColorSpace ( linear ) folder16 ( 0 ) reflect_useTexture ( off ) reflect_texture ( "" ) reflect_monoChannel ( 0 ) reflect_textureWrap ( repeat ) reflect_textureColorSpace ( linear ) folder226 ( 0 ) reflecttint_useTexture ( off ) reflecttint_texture ( "" ) reflecttint_monoChannel ( 0 ) reflecttint_textureWrap ( repeat ) reflecttint_textureColorSpace ( linear ) folder233 ( 0 ) coat_useTexture ( off ) coat_texture ( "" ) coat_monoChannel ( 0 ) coat_textureWrap ( repeat ) coat_textureColorSpace ( linear ) folder234 ( 0 ) coatrough_useTexture ( off ) coatrough_texture ( "" ) coatrough_monoChannel ( 0 ) coatrough_textureWrap ( repeat ) coatrough_textureColorSpace ( linear ) folder2 ( 0 ) transparency_useTexture ( on ) transparency_texture ( '$HIP/flowmaps/bg.tga' ) transparency_monoChannel ( 0 ) transparency_textureWrap ( repeat ) transparency_textureColorSpace ( linear ) folder5 ( 0 ) transcolor_useTexture ( off ) transcolor_texture ( "" ) transcolor_textureIntensity ( 1 ) transcolor_textureWrap ( repeat ) transcolor_textureColorSpace ( auto ) folder5_1 ( 0 ) transdist_useTexture ( off ) transdist_texture ( "" ) transdist_monoChannel ( 0 ) transdist_textureWrap ( repeat ) transdist_textureColorSpace ( linear ) folder5_2 ( 0 ) dispersion_useTexture ( off ) dispersion_texture ( "" ) dispersion_monoChannel ( 0 ) dispersion_textureWrap ( repeat ) dispersion_textureColorSpace ( linear ) folder230 ( 0 ) sss_useTexture ( off ) sss_texture ( "" ) sss_monoChannel ( 0 ) sss_textureWrap ( repeat ) sss_textureColorSpace ( linear ) folder3 ( 0 ) sssdist_useTexture ( off ) sssdist_texture ( "" ) sssdist_monoChannel ( 0 ) sssdist_textureWrap ( repeat ) sssdist_textureColorSpace ( linear ) folder3_1 ( 0 ) ssscolor_useTexture ( off ) ssscolor_texture ( "" ) ssscolor_textureWrap ( repeat ) ssscolor_textureColorSpace ( linear ) folder231 ( 0 ) sheen_useTexture ( off ) sheen_texture ( "" ) sheen_monoChannel ( 0 ) sheen_textureWrap ( repeat ) sheen_textureColorSpace ( linear ) folder232 ( 0 ) sheentint_useTexture ( off ) sheentint_texture ( "" ) sheentint_monoChannel ( 0 ) sheentint_textureWrap ( repeat ) sheentint_textureColorSpace ( linear ) diffuse_folder_14_3 ( 0 ) emitcolor_useTexture ( off ) emitcolor_texture ( "" ) emitcolor_textureIntensity ( 1 ) emitcolor_textureWrap ( repeat ) emitcolor_textureColorSpace ( repeat ) folder14 ( 0 ) opaccolor_useTexture ( off ) opaccolor_texture ( "" ) opaccolor_textureWrap ( repeat ) opaccolor_textureIntensity ( 1 ) opaccolor_textureColorSpace ( repeat ) folder18 ( 0 ) occlusion_useTexture ( off ) occlusion_texture ( "" ) occlusion_textureWrap ( repeat ) occlusion_textureIntensity ( 1 ) occlusion_textureColorSpace ( auto ) folder235 ( 0 ) surface_textureFilter ( catrom ) surface_textureFilterWidth ( 1 ) roundedEdge_enable ( off ) roundedEdge_radius ( 0.01 ) roundedEdge_mode ( 0 ) shading_16 ( 0 0 ) baseBumpAndNormal_enable ( off ) baseBumpAndNormal_type ( normal ) baseBump_colorSpace ( linear ) baseBump_bumpScale ( 0.050000000000000003 ) baseBump_bumpTexture ( "" ) baseBump_wrap ( repeat ) baseBump_filter ( gauss ) baseBump_filterWidth ( 1 ) baseBump_channel ( 0 ) baseBump_imagePlane ( "" ) baseNormal_colorspace ( linear ) baseNormal_vectorSpace ( uvtangent ) baseNormal_scale ( 1 ) baseNormal_texture ( "" ) baseNormal_wrap ( repeat ) baseNormal_filter ( gauss ) baseNormal_filterWidth ( 1 ) baseNormal_channel ( 0 ) baseNormal_imagePlane ( "" ) baseNormal_space ( 0 ) baseNormal_flipX ( off ) baseNormal_flipY ( off ) baseBump_useTexture ( baseBump_useTexture ) baseNormal_useTexture ( baseNormal_useTexture ) separateCoatNormals ( off ) coatBumpAndNormal_enable ( on ) coatBumpAndNormal_type ( normal ) coatBump_colorSpace ( linear ) coatBump_bumpScale ( 0.050000000000000003 ) coatBump_bumpTexture ( "" ) coatBump_wrap ( repeat ) coatBump_filter ( gauss ) coatBump_filterWidth ( 1 ) coatBump_channel ( 0 ) coatBump_imagePlane ( "" ) coatNormal_colorspace ( linear ) coatNormal_vectorSpace ( uvtangent ) coatNormal_scale ( 1 ) coatNormal_texture ( "" ) coatNormal_wrap ( repeat ) coatNormal_filter ( gauss ) coatNormal_filterWidth ( 1 ) coatNormal_channel ( 0 ) coatNormal_imagePlane ( "" ) coatNormal_space ( 0 ) coatNormal_flipX ( off ) coatNormal_flipY ( off ) shop_disable_displace_shader ( shop_disable_displace_shader ) folder236 ( 0 ) vm_displacebound ( vm_displacebound ) vm_truedisplace ( on ) vm_bumpraydisplace ( on ) folder10 ( 0 ) dispInput_enable ( off ) dispInput_max ( 1 ) dispInput_vectorspace ( uvtangent ) folder237 ( 0 ) dispTex_enable ( off ) dispTex_type ( disp ) dispTex_colorSpace ( linear ) dispTex_vectorSpace ( uvtangent ) dispTex_channelOrder ( xyz ) dispTex_offset ( -0.5 ) dispTex_scale ( 1 ) dispTex_texture ( "" ) dispTex_channel ( 0 ) dispTex_wrap ( repeat ) dispTex_filter ( gauss ) dispTex_filterWidth ( 1 ) folder238 ( 0 ) dispNoise_enable ( off ) dispNoise_type ( xnoise ) dispNoise_freq ( 10 10 10 ) dispNoise_offset ( 0 0 0 ) dispNoise_amp ( 1 ) dispNoise_rough ( 0.5 ) dispNoise_atten ( 1 ) dispNoise_turb ( 5 ) folder239 ( 0 ) difflabel ( diffuse ) refllabel ( reflect ) refractlabel ( refract ) coatlabel ( coat ) ssslabel ( sss ) folder0 ( 0 ) uvtrans ( 0 0 ) uvrot ( 0 ) uvscale ( 1 1 ) Cd ( 1 1 1 ) Alpha ( 1 ) layer ( "" ) direct ( 0 0 0 ) indirect ( 0 0 0 ) Ce ( 0 0 0 ) direct_emission ( 0 0 0 ) all_emission ( 0 0 0 ) all ( 0 0 0 ) indirect_emission ( 0 0 0 ) direct_comp ( "" ) indirect_comp ( "" ) all_comp ( "" ) direct_noshadow ( 0 0 0 ) direct_shadow ( 0 0 0 ) indirect_noshadow ( 0 0 0 ) indirect_shadow ( 0 0 0 ) level ( 0 ) diffuselevel ( 0 ) specularlevel ( 0 ) volumelevel ( 0 ) direct_samples ( 0 ) indirect_samples ( 0 ) nlights ( 0 ) direct_noshadow_comp ( "" ) indirect_noshadow_comp ( "" ) nddispersion ( 0 ) ndpriority ( 0 ) ndior ( 0 ) absorption ( 0 0 0 ) Oc ( 0 0 0 ) Cv ( 0 0 0 ) Th ( 0 0 0 ) Ab ( 0 0 0 ) Cu ( 0 0 0 ) Vd ( 0 0 0 ) Nt ( 0 0 0 ) Ds ( 0 0 0 ) pre_disp_P ( 0 0 0 ) pre_disp_utan ( 0 0 0 ) pre_disp_vtan ( 0 0 0 ) pre_disp_N ( 0 0 0 ) disp ( 0 ) vdisp ( 0 0 0 ) Dt ( 0 0 0 ) Vdt ( 0 0 0 ) baseN ( 0 0 0 ) coatN ( 0 0 0 ) speccolor ( speccolorr speccolorg speccolorb ) displayColor ( 1 1 1 ) st ( 0 0 ) displayOpacity ( 1 )
chlock bg_mat -*
chautoscope bg_mat -*
opcomment -c 'Created from Gallery Entry: Principled Shader\n\nPrincipled Shader (Metallic/Roughness)' bg_mat
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off -L off -M off -H on -E on bg_mat
opexprlanguage -s hscript bg_mat
opuserdata -n '___Version___' -v '' bg_mat
opuserdata -n '___toolcount___' -v '176' bg_mat
opuserdata -n '___toolid___' -v 'convertGallery' bg_mat
opuserdata -n '__inputgroup_Bump & Normals' -v 'collapsed' bg_mat
opuserdata -n '__inputgroup_Displacement' -v 'collapsed' bg_mat
opuserdata -n '__inputgroup_Opacity' -v 'collapsed' bg_mat
opuserdata -n '__inputgroup_Settings' -v 'collapsed' bg_mat
opuserdata -n '__inputgroup_Surface' -v 'collapsed' bg_mat
opuserdata -n '__inputgroup_Textures' -v 'collapsed' bg_mat
opcf ..

# Node obj (/obj)
opcf obj

# Node bg (Object/geo)
opadd -e -n geo bg
oplocate -x 3.4171976315789463 -y 1.2476128289473685 bg
opspareds '    group {         name    "stdswitcher4"         label   "Transform"          parm {             name    "xOrd"             baseparm             label   "Transform Order"             joinnext             export  none         }         parm {             name    "rOrd"             baseparm             label   "Rotate Order"             nolabel             export  none         }         parm {             name    "t"             baseparm             label   "Translate"             export  all         }         parm {             name    "r"             baseparm             label   "Rotate"             export  all         }         parm {             name    "s"             baseparm             label   "Scale"             export  none         }         parm {             name    "p"             baseparm             label   "Pivot Translate"             export  none         }         parm {             name    "pr"             baseparm             label   "Pivot Rotate"             export  none         }         parm {             name    "scale"             baseparm             label   "Uniform Scale"             export  none         }         parm {             name    "pre_xform"             baseparm             label   "Modify Pre-Transform"             export  none         }         parm {             name    "keeppos"             baseparm             label   "Keep Position When Parenting"             export  none         }         parm {             name    "childcomp"             baseparm             label   "Child Compensation"             export  none         }         parm {             name    "constraints_on"             baseparm             label   "Enable Constraints"             export  none         }         parm {             name    "constraints_path"             baseparm             label   "Constraints"             export  none         }         parm {             name    "lookatpath"             baseparm             label   "Look At"             invisible             export  none         }         parm {             name    "lookupobjpath"             baseparm             label   "Look Up Object"             invisible             export  none         }         parm {             name    "lookup"             baseparm             label   "Look At Up Vector"             invisible             export  none         }         parm {             name    "pathobjpath"             baseparm             label   "Path Object"             invisible             export  none         }         parm {             name    "roll"             baseparm             label   "Roll"             invisible             export  none         }         parm {             name    "pos"             baseparm             label   "Position"             invisible             export  none         }         parm {             name    "uparmtype"             baseparm             label   "Parameterization"             invisible             export  none         }         parm {             name    "pathorient"             baseparm             label   "Orient Along Path"             invisible             export  none         }         parm {             name    "up"             baseparm             label   "Orient Up Vector"             invisible             export  none         }         parm {             name    "bank"             baseparm             label   "Auto-Bank factor"             invisible             export  none         }     }      group {         name    "stdswitcher4_1"         label   "Render"          parm {             name    "shop_materialpath"             baseparm             label   "Material"             export  none         }         parm {             name    "shop_materialopts"             baseparm             label   "Options"             invisible             export  none         }         parm {             name    "tdisplay"             baseparm             label   "Display"             joinnext             export  none         }         parm {             name    "display"             baseparm             label   "Display"             export  none         }         parm {             name    "viewportlod"             label   "Display As"             type    ordinal             default { "full" }             help    "Choose how the object\'s geometry should be rendered in the viewport"             menu {                 "full"      "Full Geometry"                 "points"    "Point Cloud"                 "box"       "Bounding Box"                 "centroid"  "Centroid"                 "hidden"    "Hidden"                 "subd"      "Subdivision Surface / Curves"             }             parmtag { "spare_category" "Render" }         }         parm {             name    "vm_rendervisibility"             label   "Render Visibility"             type    string             default { "*" }             menureplace {                 "*"                             "Visible to all"                 "primary"                       "Visible only to primary rays"                 "primary|shadow"                "Visible only to primary and shadow rays"                 "-primary"                      "Invisible to primary rays (Phantom)"                 "-diffuse"                      "Invisible to diffuse rays"                 "-diffuse&-reflect&-refract"    "Invisible to secondary rays"                 ""                              "Invisible (Unrenderable)"             }             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "rendervisibility" }             parmtag { "spare_category" "Render" }         }         parm {             name    "vm_rendersubd"             label   "Render Polygons As Subdivision (Mantra)"             type    toggle             default { "0" }             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "rendersubd" }             parmtag { "spare_category" "Geometry" }         }         parm {             name    "vm_subdstyle"             label   "Subdivision Style"             type    string             default { "mantra_catclark" }             hidewhen "{ vm_rendersubd == 0 }"             menu {                 "mantra_catclark"   "Mantra Catmull-Clark"                 "osd_catclark"      "OpenSubdiv Catmull-Clark"             }             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "subdstyle" }             parmtag { "spare_category" "Geometry" }         }         parm {             name    "vm_subdgroup"             label   "Subdivision Group"             type    string             default { "" }             hidewhen "{ vm_rendersubd == 0 }"             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "subdgroup" }             parmtag { "spare_category" "Geometry" }         }         parm {             name    "vm_osd_quality"             label   "Open Subdiv Quality"             type    float             default { "1" }             hidewhen "{ vm_rendersubd == 0 vm_subdstyle != osd_catclark }"             range   { 0 10 }             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "osd_quality" }             parmtag { "spare_category" "Geometry" }         }         parm {             name    "vm_osd_vtxinterp"             label   "OSD Vtx Interp"             type    integer             default { "2" }             hidewhen "{ vm_rendersubd == 0 vm_subdstyle != osd_catclark }"             menu {                 "0" "No vertex interpolation"                 "1" "Edges only"                 "2" "Edges and Corners"             }             range   { 0 10 }             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "osd_vtxinterp" }             parmtag { "spare_category" "Geometry" }         }         parm {             name    "vm_osd_fvarinterp"             label   "OSD FVar Interp"             type    integer             default { "4" }             hidewhen "{ vm_rendersubd == 0 vm_subdstyle != osd_catclark }"             menu {                 "0" "Smooth everywhere"                 "1" "Sharpen corners only"                 "2" "Sharpen edges and corners"                 "3" "Sharpen edges and propagated corners"                 "4" "Sharpen all boundaries"                 "5" "Bilinear interpolation"             }             range   { 0 10 }             parmtag { "mantra_class" "object" }             parmtag { "mantra_name" "osd_fvarinterp" }             parmtag { "spare_category" "Geometry" }         }         group {             name    "folder0"             label   "Shading"              parm {                 name    "categories"                 label   "Categories"                 type    string                 default { "" }                 help    "A list of tags which can be used to select the object"                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "reflectmask"                 label   "Reflection Mask"                 type    oplist                 default { "*" }                 help    "Objects that will be reflected on this object."                 parmtag { "opexpand" "1" }                 parmtag { "opfilter" "!!OBJ/GEOMETRY!!" }                 parmtag { "oprelative" "/obj" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "refractmask"                 label   "Refraction Mask"                 type    oplist                 default { "*" }                 help    "Objects that will be refracted on this object."                 parmtag { "opexpand" "1" }                 parmtag { "opfilter" "!!OBJ/GEOMETRY!!" }                 parmtag { "oprelative" "/obj" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "lightmask"                 label   "Light Mask"                 type    oplist                 default { "*" }                 help    "Lights that illuminate this object."                 parmtag { "opexpand" "1" }                 parmtag { "opfilter" "!!OBJ/LIGHT!!" }                 parmtag { "oprelative" "/obj" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "lightcategories"                 label   "Light Selection"                 type    string                 default { "*" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "vm_lpetag"                 label   "LPE Tag"                 type    string                 default { "" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "lpetag" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "vm_volumefilter"                 label   "Volume Filter"                 type    string                 default { "box" }                 menu {                     "box"       "Box Filter"                     "gaussian"  "Gaussian"                     "bartlett"  "Bartlett (triangle)"                     "catrom"    "Catmull-Rom"                     "hanning"   "Hanning"                     "blackman"  "Blackman"                     "sinc"      "Sinc (sharpening)"                 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "filter" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "vm_volumefilterwidth"                 label   "Volume Filter Width"                 type    float                 default { "1" }                 range   { 0.001 5 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "filterwidth" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "vm_matte"                 label   "Matte shading"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "matte" }                 parmtag { "spare_category" "Shading" }             }             parm {                 name    "vm_rayshade"                 label   "Raytrace Shading"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "rayshade" }                 parmtag { "spare_category" "Shading" }             }         }          group {             name    "folder0_1"             label   "Sampling"              parm {                 name    "geo_velocityblur"                 label   "Geometry Velocity Blur"                 type    ordinal                 default { "off" }                 disablewhen "{ allowmotionblur == 0 }"                 menu {                     "off"       "No Velocity Blur"                     "on"        "Velocity Blur"                     "accelblur" "Acceleration Blur"                 }             }             parm {                 name    "geo_accelattribute"                 label   "Acceleration Attribute"                 type    string                 default { "accel" }                 hidewhen "{ geo_velocityblur != accelblur }"                 parmtag { "spare_category" "Sampling" }             }         }          group {             name    "folder0_2"             label   "Dicing"              parm {                 name    "vm_shadingquality"                 label   "Shading Quality"                 type    float                 default { "1" }                 range   { 0 10 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "shadingquality" }                 parmtag { "spare_category" "Dicing" }             }             parm {                 name    "vm_flatness"                 label   "Dicing Flatness"                 type    float                 default { "0.05" }                 range   { 0 1 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "flatness" }                 parmtag { "spare_category" "Dicing" }             }             parm {                 name    "vm_raypredice"                 label   "Ray Predicing"                 type    integer                 default { "0" }                 menu {                     "0" "Disable Predicing"                     "1" "Full Predicing"                     "2" "Precompute Bounds"                 }                 range   { 0 10 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "raypredice" }                 parmtag { "spare_category" "Dicing" }             }             parm {                 name    "vm_curvesurface"                 label   "Shade Curves As Surfaces"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "curvesurface" }                 parmtag { "spare_category" "Dicing" }             }         }          group {             name    "folder0_3"             label   "Geometry"              parm {                 name    "vm_rmbackface"                 label   "Backface Removal"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "rmbackface" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "shop_geometrypath"                 label   "Procedural Shader"                 type    oppath                 default { "" }                 parmtag { "opfilter" "!!SHOP/GEOMETRY!!" }                 parmtag { "oprelative" "." }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_forcegeometry"                 label   "Force Procedural Geometry Output"                 type    toggle                 default { "1" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_rendersubdcurves"                 label   "Render Polygon Curves As Subdivision (Mantra)"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "rendersubdcurves" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_renderpoints"                 label   "Render As Points (Mantra)"                 type    integer                 default { "2" }                 menu {                     "0" "No Point Rendering"                     "1" "Render Only Points"                     "2" "Render Unconnected Points"                 }                 range   { 0 10 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "renderpoints" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_renderpointsas"                 label   "Render Points As (Mantra)"                 type    integer                 default { "0" }                 disablewhen "{ vm_renderpoints == 0 }"                 menu {                     "0" "Spheres"                     "1" "Circles"                 }                 range   { 0 10 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "renderpointsas" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_usenforpoints"                 label   "Use N For Point Rendering"                 type    toggle                 default { "0" }                 disablewhen "{ vm_renderpoints == 0 }"                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "usenforpoints" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_pointscale"                 label   "Point Scale"                 type    float                 default { "1" }                 disablewhen "{ vm_renderpoints == 0 }"                 range   { 0! 10 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "pointscale" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_pscalediameter"                 label   "Treat Point Scale as Diameter Instead of Radius"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "pscalediameter" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_metavolume"                 label   "Metaballs as Volume"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "metavolume" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_coving"                 label   "Coving"                 type    integer                 default { "1" }                 menu {                     "0" "Disable Coving"                     "1" "Coving for displacement/sub-d"                     "2" "Coving for all primitives"                 }                 range   { 0 10 }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "coving" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_materialoverride"                 label   "Material Override"                 type    string                 default { "compact" }                 menu {                     "none"      "Disabled"                     "full"      "Evaluate for Each Primitve/Point"                     "compact"   "Evaluate Once"                 }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_overridedetail"                 label   "Ignore Geometry Attribute Shaders"                 type    toggle                 default { "0" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "overridedetail" }                 parmtag { "spare_category" "Geometry" }             }             parm {                 name    "vm_procuseroottransform"                 label   "Proc Use Root Transform"                 type    toggle                 default { "1" }                 parmtag { "mantra_class" "object" }                 parmtag { "mantra_name" "procuseroottransform" }                 parmtag { "spare_category" "Geometry" }             }         }      }      group {         name    "stdswitcher4_2"         label   "Misc"          parm {             name    "use_dcolor"             baseparm             label   "Set Wireframe Color"             export  none         }         parm {             name    "dcolor"             baseparm             label   "Wireframe Color"             export  none         }         parm {             name    "picking"             baseparm             label   "Viewport Selecting Enabled"             export  none         }         parm {             name    "pickscript"             baseparm             label   "Select Script"             export  none         }         parm {             name    "caching"             baseparm             label   "Cache Object Transform"             export  none         }         parm {             name    "vport_shadeopen"             baseparm             label   "Shade Open Curves In Viewport"             export  none         }         parm {             name    "vport_displayassubdiv"             baseparm             label   "Display as Subdivision in Viewport"             invisible             export  none         }         parm {             name    "vport_onionskin"             baseparm             label   "Onion Skinning"             export  none         }     }  ' bg
opset -S on bg
opparm -V 19.5.303 bg stdswitcher ( 0 0 0 ) xOrd ( srt ) rOrd ( xyz ) t ( 0 0 0 ) r ( 0 0 0 ) s ( 1 1 1 ) p ( 0 0 0 ) pr ( 0 0 0 ) scale ( 1 ) pre_xform ( clean ) keeppos ( off ) childcomp ( off ) constraints_on ( off ) constraints_path ( "" ) lookatpath ( "" ) lookupobjpath ( "" ) lookup ( on ) pathobjpath ( "" ) roll ( 0 ) pos ( 0 ) uparmtype ( arc ) pathorient ( 1 ) up ( 0 1 0 ) bank ( 0 ) shop_materialpath ( "" ) shop_materialopts ( override ) tdisplay ( off ) display ( 1 ) use_dcolor ( off ) dcolor ( 1 1 1 ) picking ( on ) pickscript ( "" ) caching ( on ) vport_shadeopen ( off ) vport_displayassubdiv ( off ) vport_onionskin ( off ) stdswitcher4 ( 1 1 1 ) viewportlod ( full ) vm_rendervisibility ( * ) vm_rendersubd ( off ) vm_subdstyle ( mantra_catclark ) vm_subdgroup ( "" ) vm_osd_quality ( 1 ) vm_osd_vtxinterp ( 2 ) vm_osd_fvarinterp ( 4 ) folder0 ( 0 0 0 0 ) categories ( "" ) reflectmask ( * ) refractmask ( * ) lightmask ( * ) lightcategories ( * ) vm_lpetag ( "" ) vm_volumefilter ( box ) vm_volumefilterwidth ( 1 ) vm_matte ( off ) vm_rayshade ( off ) geo_velocityblur ( off ) geo_accelattribute ( accel ) vm_shadingquality ( 1 ) vm_flatness ( 0.050000000000000003 ) vm_raypredice ( 0 ) vm_curvesurface ( off ) vm_rmbackface ( off ) shop_geometrypath ( "" ) vm_forcegeometry ( on ) vm_rendersubdcurves ( off ) vm_renderpoints ( 2 ) vm_renderpointsas ( 0 ) vm_usenforpoints ( off ) vm_pointscale ( 1 ) vm_pscalediameter ( off ) vm_metavolume ( off ) vm_coving ( 1 ) vm_materialoverride ( compact ) vm_overridedetail ( off ) vm_procuseroottransform ( on )
chlock bg -*
chautoscope bg -* +tx +ty +tz +rx +ry +rz +sx +sy +sz
opset -d on -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off -x off bg
opexprlanguage -s hscript bg
opuserdata -n '___Version___' -v '19.5.303' bg
opcf bg

# Node bg (Sop/file)
opadd -e -n file bg
oplocate -x -10.1067 -y 6.1301899999999998 bg
opspareds "" bg
chblockbegin
chadd -t 0 0 bg index
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '$FF-ch("f1")' bg/index
chblockend
opparm -V 19.5.303 bg filemode ( read ) file ( '$HIP/flowmaps/bg.fbx' ) reload ( 0 ) objpattern ( * ) geodatapath ( "" ) missingframe ( error ) loadtype ( full ) packedviewedit ( unchanged ) viewportlod ( box ) packexpanded ( on ) delayload ( off ) mkpath ( on ) cachesize ( 0 ) prefetch ( off ) f ( 1 24 ) index ( index ) wrap ( cycle ) retry ( 0 )
chlock bg -*
chautoscope bg -*
opset -d off -r off -h on -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off bg
opexprlanguage -s hscript bg
opuserdata -n '___Version___' -v '19.5.303' bg

# Node flowmap1 (labs::Sop/flowmap::2.0)
opadd -e -n labs::flowmap::2.0 flowmap1
oplocate -x 4.7868167439259253 -y -5.7757555003209768 flowmap1
opspareds "" flowmap1
opparm -V 657 flowmap1 method ( 0 ) initial_direction ( 1 0 0 ) down_vector ( 0 -1 0 ) visualize_flow ( off )
chlock flowmap1 -*
chautoscope flowmap1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off flowmap1
opexprlanguage -s hscript flowmap1
opuserdata -n '___Version___' -v '657' flowmap1

# Node flowmap_guide1 (labs::Sop/flowmap_guide)
opadd -e -n labs::flowmap_guide flowmap_guide1
oplocate -x -0.26428499999999999 -y -6.8138899999999998 flowmap_guide1
opspareds "" flowmap_guide1
opparm -V 657 flowmap_guide1 strength ( 0.10000000000000001 ) effect_width ( 1 ) falloff ( 1 ) maxsamplecount ( 10 ) bReverseDirection ( off )
chlock flowmap_guide1 -*
chautoscope flowmap_guide1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off flowmap_guide1
opexprlanguage -s hscript flowmap_guide1
opuserdata -n '___Version___' -v '657' flowmap_guide1

# Node curve2 (Sop/curve::2.0)
opadd -e -n curve::2.0 curve2
oplocate -x 1.1421300000000001 -y 1.6907300000000001 curve2
opspareds "" curve2
opparm curve2  targetorients ( 0 )
opparm curve2 group ( "" ) outputtype ( nurbs ) order ( 4 ) sepparm ( ) mode ( BUTTONS_curve_mode_draw ) interpmethod ( smoothcurvature ) operations ( "" ) delete ( 0 ) fuse ( 0 ) cut ( 0 ) join ( 0 ) deleteseg ( 0 ) makecorner ( 0 ) makesmooth ( 0 ) makebalanced ( 0 ) contract ( 0 ) expand ( 0 ) straightenseg ( 0 ) close ( 0 ) createroundedcorners ( 0 ) removeroundedcorners ( 0 ) makeauto ( 0 ) makemanual ( 0 ) reverse ( 0 ) sepparm4 ( ) sepparm5 ( ) reset ( 0 ) stashgeo ( "" ) savedstashgeo ( geometry:f05TSmJbKwALZmlsZXZlcnNpb24mACsBCDE5LjUuMzAzJgErAghoYXNpbmRleCYCMSsDCnBvaW50Y291bnQmAxEdKwQLdmVydGV4Y291bnQmBBEdKwUOcHJpbWl0aXZlY291bnQmBREBKwYEaW5mbyYGeysHBmFydGlzdCYHKwgCeGQmCCsJCHNvZnR3YXJlJgkrChBIb3VkaW5pIDE5LjUuMzAzJgorCwRkYXRlJgsrDBMyMDIyLTA4LTAzIDEyOjExOjIzJgwrDQhob3N0bmFtZSYNKw4PREVTS1RPUC1MUDBSNEpGJg4rDwZib3VuZHMmD0AZBrfPZsG/+HBBIJ1mvwB/mj/cc8s+DN34PysQEXByaW1jb3VudF9zdW1tYXJ5JhAnGCAgICAgICAgICAxIE5VUkJTIEN1cnZlCisREWF0dHJpYnV0ZV9zdW1tYXJ5JhEnuCAgICAgNCBwb2ludCBhdHRyaWJ1dGVzOglfX3B0aWR4aW5wcmltLCBfX2ludGVybmFsX3B0bnVtLCBQLCBfX3B0cHJpbWlkeAogICAgIDIgcHJpbWl0aXZlIGF0dHJpYnV0ZXM6CV9fcHJpbV9sYXN0cHRpZHgsIF9faW50ZXJuYWxfcHJpbW51bQogICAgIDEgZ2xvYmFsIGF0dHJpYnV0ZXM6CV9fcm91bmRlZGNvcm5lcnB0cworEg1ncm91cF9zdW1tYXJ5JhInTSAgICAgMSBwb2ludCBncm91cHM6CQlfX2FuY2hvcl9wb2ludHMKICAgICAxIHByaW1pdGl2ZSBncm91cHM6CV9faW5wdXRfcHJpbXMKfSsTCHRvcG9sb2d5JhNbKxQIcG9pbnRyZWYmFFsrFQdpbmRpY2VzJhVAEh0AAAEAAgADAAQABQAGAAcACAAJAAoACwAMAA0ADgAPABAAEQASABMAFAAVABYAFwAYABkAGgAbABwAXV0rFgphdHRyaWJ1dGVzJhZbKxcPcG9pbnRhdHRyaWJ1dGVzJhdbW1srGAVzY29wZSYYKxkGcHVibGljJhkrGgR0eXBlJhorGwdudW1lcmljJhsrHARuYW1lJhwrHQFQJh0rHgdvcHRpb25zJh57Jhp7JhorHwZzdHJpbmcmHysgBXZhbHVlJiArIQVwb2ludCYhfX1dWysiBHNpemUmIhEDKyMHc3RvcmFnZSYjKyQIZnByZWFsMzImJCslCGRlZmF1bHRzJiVbJiIRASYjKyYIZnByZWFsNjQmJisnBnZhbHVlcyYnQBoBAAAAAAAAAABdJidbJiIRAyYjJiQrKAhwYWdlc2l6ZSYoEgAEKykLcmF3cGFnZWRhdGEmKUAZV7fPZsEAfUQ/WfSDP71eWsEAf5o/ZfN0P4wHRsEw95E/3E9oP3lOMMHAWIg/vnhrPx7aJsGwnYU/3BhRPyZ/I8EAwYQ/RrohP/HPGsHAi4M/3HPLPuGAAcGg1G0/dlwGP1F+vcCgT0s/BbdIPwlRnMDgETo/ZPN0P6LagcDgRi4/Rhx4PxpwG8CguRQ/UyySP+b737+gKgw/HUaKP9yCSb/Ajfw+/26NP9QVmT5AFd4+/26NP1KEvz8AV7s+UiySP1yzMkDAgpM+TWSgPyr7hUAAD1E+uDCwPx5rokCAoRM+WePHPxPbvkAACLE9ptjaP9Dy1kAAlCY9TVPkP4uY8kAA4MG73L7iPyACEEEAEp+9UxvWP1WvGUEA2t+9cfLSP4kjJUEAfR2+iQHeP0wDL0EA+EO+vuflPyhkQEHAJ4O+1vbwP+mYT0HAvp++DN34P7/4cEEgnWa/RovyP11dXVtbJhgmGSYaJhsmHCsqEF9faW50ZXJuYWxfcHRudW0mKiYeeyYaeyYaJh8mICsrFW5vbmFyaXRobWV0aWNfaW50ZWdlciYrfX1dWyYiEQEmIyssBWludDMyJiwmJVsmIhEBJiMrLQVpbnQ2NCYtJidAFAH//////////10mJ1smIhEBJiMmLCYoEgAEJilAEx0AAAAAAQAAAAIAAAADAAAABAAAAAUAAAAGAAAABwAAAAgAAAAJAAAACgAAAAsAAAAMAAAADQAAAA4AAAAPAAAAEAAAABEAAAASAAAAEwAAABQAAAAVAAAAFgAAABcAAAAYAAAAGQAAABoAAAAbAAAAHAAAAF1dXVtbJhgmGSYaJhsmHCsuDV9fcHRpZHhpbnByaW0mLiYeeyYaeyYaJh8mICYrfX1dWyYiEQEmIyYsJiVbJiIRASYjJi0mJ0AUAf//////////XSYnWyYiEQEmIyYsJigSAAQmKUATHQAAAAABAAAAAgAAAAMAAAAEAAAABQAAAAYAAAAHAAAACAAAAAkAAAAKAAAACwAAAAwAAAANAAAADgAAAA8AAAAQAAAAEQAAABIAAAATAAAAFAAAABUAAAAWAAAAFwAAABgAAAAZAAAAGgAAABsAAAAcAAAAXV1dW1smGCYZJhomGyYcKy8LX19wdHByaW1pZHgmLyYeeyYaeyYaJh8mICYrfX1dWyYiEQEmIyYsJiVbJiIRASYjJi0mJ0AUAf//////////XSYnWyYiEQEmIyYsJigSAAQrMBFjb25zdGFudHBhZ2VmbGFncyYwW0AQAQEAAABdJilAEwEAAAAAXV1dXSsxE3ByaW1pdGl2ZWF0dHJpYnV0ZXMmMVtbWyYYJhkmGiYbJhwrMhJfX2ludGVybmFsX3ByaW1udW0mMiYeeyYaeyYaJh8mICYrfX1dWyYiEQEmIyYsJiVbJiIRASYjJi0mJ0AUAf//////////XSYnWyYiEQEmIyYsJigSAAQmMFtAEAEBAAAAXSYpQBMBAAAAAF1dXVtbJhgmGSYaJhsmHCszEF9fcHJpbV9sYXN0cHRpZHgmMyYeeyYaeyYaJh8mICYrfX1dWyYiEQEmIyYsJiVbJiIRASYjJi0mJ0AUAf//////////XSYnWyYiEQEmIyYsJigSAAQmMFtAEAEBAAAAXSYpQBMBHAAAAF1dXV0rNBBnbG9iYWxhdHRyaWJ1dGVzJjRbW1smGCYZJhomHyYcKzUSX19yb3VuZGVkY29ybmVycHRzJjUmHnt9XVsmIhEBJiMmLCs2B3N0cmluZ3MmNltdJhVbJiIRASYjJiwmKBIABCYwW0AQAQEAAABdJilAEwH/////XV1dXV0rNwpwcmltaXRpdmVzJjdbW1smGis4CU5VUkJDdXJ2ZSY4XVsrOQZ2ZXJ0ZXgmOUASHQAAAQACAAMABAAFAAYABwAIAAkACgALAAwADQAOAA8AEAARABIAEwAUABUAFgAXABgAGQAaABsAHAArOgZjbG9zZWQmOjArOwViYXNpcyY7WyYaKzwFTlVSQlMmPCs9BW9yZGVyJj0RBCs+EGVuZGludGVycG9sYXRpb24mPjErPwVrbm90cyY/QBohAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUO7ETO7GjPxQ7sRM7sbM/ntiJndiJvT8UO7ETO7HDP9mJndiJncg/ntiJndiJzT+yEzuxEzvRPxQ7sRM7sdM/dmIndmIn1j/ZiZ3YiZ3YPzyxEzuxE9s/ntiJndiJ3T8AAAAAAADgP7ITO7ETO+E/Yyd2Yid24j8UO7ETO7HjP8VO7MRO7OQ/dmIndmIn5j8odmIndmLnP9mJndiJneg/ip3YiZ3Y6T88sRM7sRPrP+3ETuzETuw/ntiJndiJ7T9P7MRO7MTuPwAAAAAAAPA/AAAAAAAA8D8AAAAAAADwPwAAAAAAAPA/XV1dXStAC3BvaW50Z3JvdXBzJkBbW1smHCtBD19fYW5jaG9yX3BvaW50cyZBXVsrQglzZWxlY3Rpb24mQlsrQwl1bm9yZGVyZWQmQ1srRAdib29sUkxFJkRbER0xXV1dXV1dK0UPcHJpbWl0aXZlZ3JvdXBzJkVbW1smHCtGDV9faW5wdXRfcHJpbXMmRl1bJkJbJkNbK0cCaTgmR0ARAQFdXV1dXScFaW5kZXhbJw5pbnRlZ2VyZW50cmllc3t9Jw1zdHJpbmdlbnRyaWVze30nEWludGVnZXJrZXllbnRpcmVze30nEHN0cmluZ2tleWVudHJpZXN7fV0nDWluZGV4cG9zaXRpb24UZgAAAAAAAABd ) parmpoints ( 'geometry:f05TSmJbKwALZmlsZXZlcnNpb24mACsBCDE5LjUuMzAzJgErAghoYXNpbmRleCYCMSsDCnBvaW50Y291bnQmAxFXKwQLdmVydGV4Y291bnQmBBEAKwUOcHJpbWl0aXZlY291bnQmBREAKwYEaW5mbyYGeysHBmFydGlzdCYHKwgCeGQmCCsJCHNvZnR3YXJlJgkrChBIb3VkaW5pIDE5LjUuMzAzJgorCwRkYXRlJgsrDBMyMDIyLTA4LTAzIDEyOjExOjIzJgwrDQhob3N0bmFtZSYNKw4PREVTS1RPUC1MUDBSNEpGJg4rDwZib3VuZHMmD0AZBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsQEWF0dHJpYnV0ZV9zdW1tYXJ5JhAn8rABICAgIDMwIHBvaW50IGF0dHJpYnV0ZXM6CWFjdGl2ZXBvaW50cywgc2NhbGUsIGFwcHJveGVuZHRhbmdlbnRzLCBtYWludGFpbnBvaW50LCBjb25zdHJhaW5zdHJhaWdodGVuLCBwbGFuZW9yaWcsIHNtb290aHB0cywgaGFuZGxlcDBwb3MsIHNlZ21lbnRiZXppZXJjbG9zZSwgY29ybmVycHRzLCByb3VuZGNvcm5lcnJhZGl1cywgYXV0b3B0cywgaGFuZGxlcDFwb3MsIGhhbmRsZXB0cywgcGxhbmVubWwsIHBpdm90dHJhbnNsYXRlLCByb3VuZGNvcm5lcnB0cywgaGFuZGxlcDJwb3MsIG9yZGVyLCBwaXZvdHJvdGF0ZSwgb3B0eXBlLCBhdXRvYmV6aWVyLCBncm91cHNpbmdsZXBvaW50LCBpbnRlcnBtZXRob2QsIGFkZHB0cywgb3V0cHV0dHlwZSwgdHJhbnNsYXRlLCBQLCBhY3RpdmVwcmltLCByb3RhdGUKICAgICAxIGdsb2JhbCBhdHRyaWJ1dGVzOgl2YXJtYXAKfSsRCHRvcG9sb2d5JhFbKxIIcG9pbnRyZWYmElsrEwdpbmRpY2VzJhNAEgBdXSsUCmF0dHJpYnV0ZXMmFFsrFQ9wb2ludGF0dHJpYnV0ZXMmFVtbWysWBXNjb3BlJhYrFwZwdWJsaWMmFysYBHR5cGUmGCsZB251bWVyaWMmGSsaBG5hbWUmGisbAVAmGyscB29wdGlvbnMmHHsmGHsmGCsdBnN0cmluZyYdKx4FdmFsdWUmHisfBXBvaW50Jh99fV1bKyAEc2l6ZSYgEQMrIQdzdG9yYWdlJiErIghmcHJlYWwzMiYiKyMIZGVmYXVsdHMmI1smIBEBJiErJAhmcHJlYWw2NCYkKyUGdmFsdWVzJiVAGgEAAAAAAAAAAF0mJVsmIBEDJiEmIismCHBhZ2VzaXplJiYSAAQrJxFjb25zdGFudHBhZ2VmbGFncyYnW0AQAQEAAABdKygLcmF3cGFnZWRhdGEmKEAZAwAAAAAAAAAAAAAAAF1dXVtbJhYmFyYYJh0mGispDGFjdGl2ZXBvaW50cyYpJhx7fV1bJiARASYhKyoFaW50MzImKisrB3N0cmluZ3MmK1srLAE1JiwrLQE2Ji0rLgE0Ji5dJhNbJiARASYhJiomJhIABCYoQBNX//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAAAAAAAAAAEAAAABAAAAAQAAAAAAAAABAAAAAQAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAACAAAAAgAAAAIAAAACAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAgAAAAIAAAACAAAAAgAAAAIAAAAAAAAAAAAAAAAAAAABAAAAXV1dW1smFiYXJhgmGSYaKy8KYWN0aXZlcHJpbSYvJhx7Jhh7JhgmHSYeKzAVbm9uYXJpdGhtZXRpY19pbnRlZ2VyJjB9fV1bJiARASYhJiomI1smIBEBJiErMQVpbnQ2NCYxJiVAFAEAAAAAAAAAAF0mJVsmIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAQAAAABdXV1bWyYWJhcmGCYdJhorMgZhZGRwdHMmMiYce31dWyYgEQEmISYqJitbKzPmLTE0LjQyNTcxMTYzMTc3NDkwMiwwLjc2NzUzMjM0ODYzMjkzNjgsMS4wMzA4OTQzNjE0ODgyMDgyIC0xMy42NDgxMjk0NjMxOTU4LDEuMjA3MDAwNzMyNDIxOTk1OCwwLjk1NjgzODkyMDk5ODk2MTggLTEyLjM3Njg0MjQ5ODc3OTI5NywxLjE0MDM1NjA2Mzg0Mjg5NDIsMC45MDc0Njg1NjQ2MDA3NzY1IC0xMS4wMTkxNTgzNjMzNDIyODUsMS4wNjUyMDg0MzUwNTg3MTQ1LDAuOTE5ODExMTI3ODU0NzAwMyAmMys0OS0xMC41NzQ4MjQzMzMxOTA5MTgsMS4wNDM4NzQ3NDA2MDA3MDY3LDAuODcwNDQwNzc2ODU4MDA0ICY0KzU6LTEwLjIwNDU0NjkyODQwNTc2MiwxLjAzNzEzOTg5MjU3ODI0NTgsMC42NDgyNzQyMzc4MTY2MjIyICY1KzY4LTkuNzg0ODk4NzU3OTM0NTcsMS4wMjc3MDIzMzE1NDMwODk1LDAuNDI2MTA3NjM4ODQ4NDA3MSAmNis3OS04LjA5Mzk2NDU3NjcyMTE5MSwwLjkyOTAyNTY1MDAyNDUzNDksMC41MjQ4NDgzMjM5OTIyNzM0ICY3Kzg6LTUuOTIxNjY5NDgzMTg0ODE0NSwwLjc5NDE4MzczMTA3OTIyNTksMC43ODQwNDI2ODIwMDIzMzY3ICY4Kzk5LTQuODg0ODkxOTg2ODQ2OTI0LDAuNzI2ODM1MjUwODU0NjE2NSwwLjk1NjgzODg2Mzc1ODc3NTkgJjkrOjgtNC4wNTc5Mzg1NzU3NDQ2MjksMC42ODA3Njg5NjY2NzQ5MjksMC45NjkxODE0MzA0Nzk0NjYxICY6Kzs6LTIuNDI4NzE3MTM2MzgzMDU2NiwwLjU4MDk1NzQxMjcxOTg1MDksMS4xNDE5Nzc2MDgzNjYwMDU0ICY7Kzw6LTEuNzQ5ODc0ODMwMjQ1OTcxNywwLjU0NzUyNTQwNTg4MzkxMzQsMS4wODAyNjQ3NDMzMTk1MDM0ICY8Kz07LTAuNzg3MTUzMDA1NTk5OTc1NiwwLjQ5MzI2ODk2NjY3NDkyOTAzLDEuMTA0OTQ5ODgxMjc2MDcwMyAmPSs+OjAuMjk4OTk0NjYwMzc3NTAyNDQsMC40MzM3NTU4NzQ2MzM5MTM0LDEuMTA0OTQ5ODc0MTgxNTU2OSAmPis/OTEuNDk2MjI1NTk1NDc0MjQzMiwwLjM2NTg5ODEzMjMyNDM0MzEsMS4xNDE5Nzc1ODI3Mjg5NDEzICY/K0A6Mi43OTIxOTcyMjc0NzgwMjczLDAuMjg4MTA2OTE4MzM1MDg4ODQsMS4yNTMwNjA5NjE3ODQwNTgyICZAK0E6NC4xODY5MDk2NzU1OTgxNDQ1LDAuMjA0MTU4NzgyOTU5MTEyMjcsMS4zNzY0ODY3OTMxMDgxNDI2ICZBK0I5NS4wNzU1NzU4Mjg1NTIyNDYsMC4xNDQxNzA3NjExMDg1MjYzNCwxLjU2MTYyNTYwNzU1ODg4MDggJkIrQzg1Ljk2NDI0MjQ1ODM0MzUwNiwwLjA4NjQ0MTA0MDAzOTE5MDQsMS43MDk3MzY1ODY0MzI4ODIyICZDK0Q5Ni43MTcxNDAxOTc3NTM5MDYsMC4wNDA2Njg0ODc1NDg5NTYwMiwxLjc4Mzc5MjEzMzQ1ODk3MzcgJkQrRTo3LjU4MTEyMDk2Nzg2NDk5LC0wLjAwNTkxNjU5NTQ1ODg1NjQ3NywxLjc3MTQ0OTU1NTY5MzM3OTggJkUrRjo5LjAwMDUxODc5ODgyODEyNSwtMC4wNzc2NzEwNTEwMjUyNjI3MywxLjY3MjcwODg1MDIzMjUzODcgJkYrRzo5LjYwNTMwNTY3MTY5MTg5NSwtMC4xMDkzMDI1MjA3NTE4MjUyMywxLjY0ODAyMzcwMjAzNzMzNTIgJkcrSDsxMC4zMjExNzU1NzUyNTYzNDgsLTAuMTUzNzk3MTQ5NjU4MDc1MjMsMS43MzQ0MjE4MjE0Mjc5ODYzICZIK0k7MTAuOTM4MzA0OTAxMTIzMDQ3LC0wLjE5MTM3NTczMjQyMTc0MzU1LDEuNzk2MTM0Njc4MDA5MzY2MyAmSStKOjEyLjAyNDQ1MjIwOTQ3MjY1NiwtMC4yNTYxNjI2NDM0MzI0ODU3NCwxLjg4MjUzMjc5NDk4MDk4OSAmSitLOTEyLjk3NDgzMTU4MTExNTcyMywtMC4zMTIwMDIxODIwMDY3MDQ1LDEuOTQ0MjQ1NzY4NTk0NzgzICZLK0w4MTUuMDYwNzI5MDI2Nzk0NDM0LC0wLjkwMDgzNTAzNzIzMTMxMDMsMS44OTQ4NzUyOTAzNDIyNyAmTF0mE1smIBEBJiEmKiYmEgAEJihAE1cAAAAAAQAAAAIAAAADAAAABAAAAAUAAAAGAAAABwAAAAgAAAAJAAAACgAAAAsAAAAMAAAADQAAAA4AAAAPAAAAEAAAABEAAAASAAAAEwAAABQAAAAVAAAAFgAAABcAAAAYAAAAGQAAAP////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////9dXV1bWyYWJhcmGCYZJhorTRFhcHByb3hlbmR0YW5nZW50cyZNJhx7Jhh7JhgmHSYeJjB9fV1bJiARASYhJiomI1smIBEBJiEmMSYlQBQBAAAAAAAAAABdJiVbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwEAAAAAXV1dW1smFiYXJhgmGSYaK04KYXV0b2JlemllciZOJhx7Jhh7JhgmHSYeJjB9fV1bJiARASYhJiomI1smIBEBJiEmMSYlQBQBAAAAAAAAAABdJiVbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwEAAAAAXV1dW1smFiYXJhgmHSYaK08HYXV0b3B0cyZPJhx7fV1bJiARASYhJiomK1tdJhNbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwH/////XV1dW1smFiYXJhgmGSYaK1ATY29uc3RyYWluc3RyYWlnaHRlbiZQJhx7Jhh7JhgmHSYeJjB9fV1bJiARASYhJiomI1smIBEBJiEmMSYlQBQBAAAAAAAAAABdJiVbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwEAAAAAXV1dW1smFiYXJhgmHSYaK1EJY29ybmVycHRzJlEmHHt9XVsmIBEBJiEmKiYrW10mE1smIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAf////9dXV1bWyYWJhcmGCYZJhorUhBncm91cHNpbmdsZXBvaW50JlImHHsmGHsmGCYdJh4mMH19XVsmIBEBJiEmKiYjWyYgEQEmISYxJiVAFAEAAAAAAAAAAF0mJVsmIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAQEAAABdXV1bWyYWJhcmGCYZJhorUwtoYW5kbGVwMHBvcyZTJhx7fV1bJiARAyYhJiImI1smIBEBJiEmJCYlQBoBAAAAAAAAAABdJiVbJiARAyYhJiImJhIABCYnW0AQAQEAAABdJihAGQMAAIC/AAAAAAAAAABdXV1bWyYWJhcmGCYZJhorVAtoYW5kbGVwMXBvcyZUJhx7fV1bJiARAyYhJiImI1smIBEBJiEmJCYlQBoBAAAAAAAAAABdJiVbJiARAyYhJiImJhIABCYnW0AQAQEAAABdJihAGQMAAAAAAAAAAAAAAABdXV1bWyYWJhcmGCYZJhorVQtoYW5kbGVwMnBvcyZVJhx7fV1bJiARAyYhJiImI1smIBEBJiEmJCYlQBoBAAAAAAAAAABdJiVbJiARAyYhJiImJhIABCYnW0AQAQEAAABdJihAGQMAAIA/AAAAAAAAAABdXV1bWyYWJhcmGCYZJhorVgloYW5kbGVwdHMmViYceyYYeyYYJh0mHiYwfX1dWyYgEQMmISYqJiNbJiARASYhJjEmJUAUAf//////////XSYlWyYgEQMmISYqJiYSAAQmJ1tAEAEBAAAAXSYoQBMD////////////////XV1dW1smFiYXJhgmGSYaK1cMaW50ZXJwbWV0aG9kJlcmHHsmGHsmGCYdJh4mMH19XVsmIBEBJiEmKiYjWyYgEQEmISYxJiVAFAEAAAAAAAAAAF0mJVsmIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAQAAAABdXV1bWyYWJhcmGCYZJhorWA1tYWludGFpbnBvaW50JlgmHHsmGHsmGCYdJh4mMH19XVsmIBEBJiEmKiYjWyYgEQEmISYxJiVAFAEAAAAAAAAAAF0mJVsmIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAQAAAABdXV1bWyYWJhcmGCYdJhorWQZvcHR5cGUmWSYce31dWyYgEQEmISYqJitbK1oLYXBwZW5kcG9pbnQmWitbCXRyYW5zZm9ybSZbXSYTWyYgEQEmISYqJiYSAAQmKEATVwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAF1dXVtbJhYmFyYYJhkmGitcBW9yZGVyJlwmHHsmGHsmGCYdJh4mMH19XVsmIBEBJiEmKiYjWyYgEQEmISYxJiVAFAEEAAAAAAAAAF0mJVsmIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAQQAAABdXV1bWyYWJhcmGCYdJhorXQpvdXRwdXR0eXBlJl0mHHt9XVsmIBEBJiEmKiYrWyteBW51cmJzJl5dJhNbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwEAAAAAXV1dW1smFiYXJhgmGSYaK18LcGl2b3Ryb3RhdGUmXyYce31dWyYgEQMmISYiJiNbJiARASYhJiQmJUAaAQAAAAAAAAAAXSYlWyYgEQMmISYiJiYSAAQmJ1tAEAEBAAAAXSYoQBkDAAAAAAAAAAAAAAAAXV1dW1smFiYXJhgmGSYaK2AOcGl2b3R0cmFuc2xhdGUmYCYce31dWyYgEQMmISYiJiNbJiARASYhJiQmJUAaAQAAAAAAAAAAXSYlWyYgEQMmISYiJiYSAAQmJ1tAEAEBAAAAXSYoQBkDAAAAAAAAAAAAAAAAXV1dW1smFiYXJhgmGSYaK2EIcGxhbmVubWwmYSYce31dWyYgEQMmISYiJiNbJiARASYhJiQmJUAaAQAAAAAAAAAAXSYlWyYgEQMmISYiJiYSAAQmJ1tAEAEBAAAAXSYoQBkDAAAAAAAAgD8AAAAAXV1dW1smFiYXJhgmGSYaK2IJcGxhbmVvcmlnJmImHHt9XVsmIBEDJiEmIiYjWyYgEQEmISYkJiVAGgEAAAAAAAAAAF0mJVsmIBEDJiEmIiYmEgAEJidbQBABAQAAAF0mKEAZAwAAAAAAAAAAAAAAAF1dXVtbJhYmFyYYJhkmGitjBnJvdGF0ZSZjJhx7fV1bJiARAyYhJiImI1smIBEBJiEmJCYlQBoBAAAAAAAAAABdJiVbJiARAyYhJiImJhIABCYnW0AQAQEAAABdJihAGQMAAAAAAAAAAAAAAABdXV1bWyYWJhcmGCYdJhorZA5yb3VuZGNvcm5lcnB0cyZkJhx7fV1bJiARASYhJiomK1tdJhNbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwH/////XV1dW1smFiYXJhgmGSYaK2URcm91bmRjb3JuZXJyYWRpdXMmZSYce31dWyYgEQEmISYiJiNbJiARASYhJiQmJUAaAQAAAAAAAAAAXSYlWyYgEQEmISYiJiYSAAQmJ1tAEAEBAAAAXSYoQBkBAAAAAF1dXVtbJhYmFyYYJhkmGitmBXNjYWxlJmYmHHt9XVsmIBEDJiEmIiYjWyYgEQEmISYkJiVAGgEAAAAAAADwP10mJVsmIBEDJiEmIiYmEgAEJidbQBABAQAAAF0mKEAZAwAAgD8AAIA/AACAP11dXVtbJhYmFyYYJhkmGitnEnNlZ21lbnRiZXppZXJjbG9zZSZnJhx7Jhh7JhgmHSYeJjB9fV1bJiARASYhJiomI1smIBEBJiEmMSYlQBQBAAAAAAAAAABdJiVbJiARASYhJiomJhIABCYnW0AQAQEAAABdJihAEwEBAAAAXV1dW1smFiYXJhgmHSYaK2gJc21vb3RocHRzJmgmHHt9XVsmIBEBJiEmKiYrWytpASomaV0mE1smIBEBJiEmKiYmEgAEJidbQBABAQAAAF0mKEATAQAAAABdXV1bWyYWJhcmGCYZJhoragl0cmFuc2xhdGUmaiYce31dWyYgEQMmISYiJiNbJiARASYhJiQmJUAaAQAAAAAAAAAAXSYlWyYgEQMmISYiJiYSAAQmKEAZ8gUBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwIMiPgAAAACAS4m7AAAAAAAAAAAAAAAAwAtMvgAAAACzK3S+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKP8vQAAAACgX+M9wL1WvgAAAAAQHEo8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQBxKvgAAAACX2LC+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBzKPgAAAAAYHMo9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGTgPQAAAAD0iAY+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADckPgAAAAAgHEq8QAFwPgAAAAAzRJE+gNiwPQAAAAByooQ+AJUXPQAAAABYej2+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABzKvQAAAAAgHMq8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgF/jvQAAAABYej2+XV1dXStrEGdsb2JhbGF0dHJpYnV0ZXMma1tbWyYWJhcmGCYdJhorbAZ2YXJtYXAmbCYceyttDmJsb2I6c3RyaW5nc2V0Jm17JhgrbgRib29sJm4mHjF9fV1bJiARHSYhJiomK1srbxBvcHR5cGUgLT4gT1BUWVBFJm8rcBxhY3RpdmVwb2ludHMgLT4gQUNUSVZFUE9JTlRTJnArcSRncm91cHNpbmdsZXBvaW50IC0+IEdST1VQU0lOR0xFUE9JTlQmcStyEGFkZHB0cyAtPiBBRERQVFMmcitzGGFjdGl2ZXByaW0gLT4gQUNUSVZFUFJJTSZzK3QebWFpbnRhaW5wb2ludCAtPiBNQUlOVEFJTlBPSU5UJnQrdSZhcHByb3hlbmR0YW5nZW50cyAtPiBBUFBST1hFTkRUQU5HRU5UUyZ1K3YYb3V0cHV0dHlwZSAtPiBPVVRQVVRUWVBFJnYrdw5vcmRlciAtPiBPUkRFUiZ3K3gmcm91bmRjb3JuZXJyYWRpdXMgLT4gUk9VTkRDT1JORVJSQURJVVMmeCt5FmNvcm5lcnB0cyAtPiBDT1JORVJQVFMmeSt6FnNtb290aHB0cyAtPiBTTU9PVEhQVFMmeit7EmF1dG9wdHMgLT4gQVVUT1BUUyZ7K3wgcm91bmRjb3JuZXJwdHMgLT4gUk9VTkRDT1JORVJQVFMmfCt9KmNvbnN0cmFpbnN0cmFpZ2h0ZW4gLT4gQ09OU1RSQUlOU1RSQUlHSFRFTiZ9K34caW50ZXJwbWV0aG9kIC0+IElOVEVSUE1FVEhPRCZ+K38WdHJhbnNsYXRlIC0+IFRSQU5TTEFURSZ/K4AQcm90YXRlIC0+IFJPVEFURSaAK4EOc2NhbGUgLT4gU0NBTEUmgSuCIHBpdm90dHJhbnNsYXRlIC0+IFBJVk9UVFJBTlNMQVRFJoIrgxpwaXZvdHJvdGF0ZSAtPiBQSVZPVFJPVEFURSaDK4QWaGFuZGxlcHRzIC0+IEhBTkRMRVBUUyaEK4UaaGFuZGxlcDBwb3MgLT4gSEFORExFUDBQT1MmhSuGGmhhbmRsZXAxcG9zIC0+IEhBTkRMRVAxUE9TJoYrhxpoYW5kbGVwMnBvcyAtPiBIQU5ETEVQMlBPUyaHK4gWcGxhbmVvcmlnIC0+IFBMQU5FT1JJRyaIK4kUcGxhbmVubWwgLT4gUExBTkVOTUwmiSuKGGF1dG9iZXppZXIgLT4gQVVUT0JFWklFUiaKK4soc2VnbWVudGJlemllcmNsb3NlIC0+IFNFR01FTlRCRVpJRVJDTE9TRSaLXSYTWyYgER0mISYqJiYSAAQmJ1tAEAEBAAAAXSYoQBMdAAAAAAEAAAACAAAAAwAAAAQAAAAFAAAABgAAAAcAAAAIAAAACQAAAAoAAAALAAAADAAAAA0AAAAOAAAADwAAABAAAAARAAAAEgAAABMAAAAUAAAAFQAAABYAAAAXAAAAGAAAABkAAAAaAAAAGwAAABwAAABdXV1dXSuMCnByaW1pdGl2ZXMmjFtdJwVpbmRleFsnDmludGVnZXJlbnRyaWVze30nDXN0cmluZ2VudHJpZXN7fScRaW50ZWdlcmtleWVudGlyZXN7fScQc3RyaW5na2V5ZW50cmllc3t9XScNaW5kZXhwb3NpdGlvbhRmAAAAAAAAAF0=' ) stashid ( ; ) ninputprimscached ( 0 ) snaponclose ( on ) maintainpoint ( off ) approxendtangents ( off ) sepparm2 ( ) outputcornerpts ( off ) cornerptsgroup ( corner_points ) outputsmoothpts ( off ) smoothptsgroup ( smooth_points ) outputautopts ( off ) autoptsgroup ( auto_points ) outputname ( off ) name ( '$OS' ) nameattrib ( name ) outputxaxis ( off ) xaxisname ( out ) outputyaxis ( off ) yaxisname ( up ) outputzaxis ( off ) zaxisname ( tangent ) outputorient ( off ) orientname ( orient ) tangenttype ( avgdir ) aligntangent ( on ) targetorients ( 0 ) curoperation ( 0 ) optype ( appendpoint ) activepoints ( 6 ) groupsinglepoint ( on ) translate ( 0 0 0 ) rotate ( 0 0 0 ) scale ( 1 1 1 ) addpts ( "" ) activeprim ( 0 ) cornerpts ( "" ) smoothpts ( * ) autopts ( "" ) pivottransformfolder ( 0 ) pivottranslate ( 0 0 0 ) pivotfixed ( off ) pivotrotate ( 0 0 0 ) roundedcornersfolder ( 0 ) viewroundcorners ( on ) roundcornerpts ( "" ) roundcornerradius ( 0 ) bakeroundcorners ( 0 ) showroundcornerwidget ( on ) showroundcornerlabels ( on ) editedgeops ( 0 ) planeorig ( 0 0 0 ) planenml ( 0 1 0 ) constrainstraighten ( off ) handleparmsfolder ( 0 ) handlepts ( -1 -1 -1 ) handlep0pos ( -1 0 0 ) handlep1pos ( 0 0 0 ) handlep2pos ( 1 0 0 )
chlock curve2 -*
chautoscope curve2 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off curve2
opexprlanguage -s hscript curve2
opuserdata -n '___Version___' -v '' curve2
opuserdata -n '___toolcount___' -v '1' curve2
opuserdata -n '___toolid___' -v 'sop_curve' curve2

# Node flowmap_to_color1 (labs::Sop/flowmap_to_color)
opadd -e -n labs::flowmap_to_color flowmap_to_color1
oplocate -x -0.26428499999999999 -y -8.8138900000000007 flowmap_to_color1
opspareds "" flowmap_to_color1
opparm flowmap_to_color1  num_visualizers ( 0 )
opparm -V 657 flowmap_to_color1 node_vis_enabled ( on ) num_visualizers ( 0 ) flip_g ( on )
chlock flowmap_to_color1 -*
chautoscope flowmap_to_color1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off flowmap_to_color1
opexprlanguage -s hscript flowmap_to_color1
opuserdata -n '___Version___' -v '657' flowmap_to_color1

# Node ray1 (Sop/ray)
opadd -e -n ray ray1
oplocate -x -0.52210100000000004 -y -0.68534899999999999 ray1
opspareds "" ray1
chblockbegin
chadd -t 0 0 ray1 dirx
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F @N.x ray1/dirx
chadd -t 0 0 ray1 diry
chkey -t 0 -v -1 -m 0 -a 0 -A 0 -T a  -F @N.y ray1/diry
chadd -t 0 0 ray1 dirz
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F @N.z ray1/dirz
chblockend
opparm -V 19.5.303 ray1 group ( "" ) entity ( primitive ) collision ( "" ) method ( minimum ) dirmethod ( vector ) dir ( dirx diry dirz ) dirattrib ( N ) showguide ( on ) dotrans ( on ) lookfar ( off ) putnml ( off ) putdist ( off ) reverserays ( on ) rtolerance ( 0.01 ) scale ( 1 ) lift ( 0 ) bias ( 0 ) maxraydistcheck ( off ) maxraydist ( 0 ) sample ( 1 ) jitter ( 0 ) combinetype ( average ) seed ( 1 ) newgrp ( off ) hitgrp ( rayHitGroup ) useprimnumattrib ( off ) primnumattrib ( hitprim ) useprimuvwattrib ( off ) primuvwattrib ( hitprimuv ) getptattribs ( off ) ptattribnames ( * ) vertexattribnames ( "" ) primitiveattribnames ( "" ) detailattribnames ( "" )
chlock ray1 -*
chautoscope ray1 -*
opset -d off -r off -h off -f off -y off -t on -l off -s off -u off -F on -c on -e on -b off ray1
opexprlanguage -s hscript ray1
opuserdata -n '___Version___' -v '19.5.303' ray1

# Node transform1 (Sop/xform)
opadd -e -n xform transform1
oplocate -x -8.4499099999999991 -y 1.93445 transform1
opspareds "" transform1
opparm -V 19.5.303 transform1 group ( "" ) grouptype ( guess ) xOrd ( srt ) rOrd ( yxz ) t ( 1524.0324117124262 212.34434725101369 -1709.0422222286768 ) r ( 60.000000000000021 45.000000000000028 0 ) s ( 1 1 1 ) shear ( 0 0 0 ) scale ( 0.01 ) parmgroup_pivotxform ( 1 ) p ( -1536.739990234375 -78.708137512207031 1709.042236328125 ) pr ( 0 0 -5 ) parmgroup_prexform ( 0 ) prexform_xOrd ( srt ) prexform_rOrd ( xyz ) prexform_t ( 0 0 0 ) prexform_r ( 0 0 0 ) prexform_s ( 1 1 1 ) prexform_shear ( 0 0 0 ) movecentroid ( 0 ) attribs ( * ) updatenmls ( off ) updateaffectednmls ( on ) vlength ( on ) invertxform ( off ) addattrib ( off ) outputattrib ( xform ) outputmerge ( post )
chlock transform1 -*
chautoscope transform1 -* +tx +ty +tz +rx +ry +rz +sx +sy +sz
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off transform1
opexprlanguage -s hscript transform1
opuserdata -n '___Version___' -v '19.5.303' transform1

# Node blast1 (Sop/blast)
opadd -e -n blast blast1
oplocate -x -4.9480300000000002 -y 0.79973099999999997 blast1
opspareds "" blast1
opparm -V 19.5.303 blast1 group ( '0 1 2 3' ) grouptype ( prims ) computenorms ( off ) negate ( off ) fillhole ( off ) removegrp ( on )
chlock blast1 -*
chautoscope blast1 -*
opset -d off -r off -h on -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off blast1
opexprlanguage -s hscript blast1
opuserdata -n '___Version___' -v '19.5.303' blast1
opuserdata -n '___toolcount___' -v '7' blast1
opuserdata -n '___toolid___' -v 'generic_delete' blast1

# Node RiverMesh (Sop/null)
opadd -e -n null RiverMesh
oplocate -x -0.26083499999999998 -y -11.271000000000001 RiverMesh
opspareds "" RiverMesh
opparm -V 19.5.303 RiverMesh copyinput ( on ) cacheinput ( off )
chlock RiverMesh -*
chautoscope RiverMesh -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off RiverMesh
opexprlanguage -s hscript RiverMesh
opuserdata -n '___Version___' -v '19.5.303' RiverMesh

# Node attribpromote1 (Sop/attribpromote)
opadd -e -n attribpromote attribpromote1
oplocate -x -0.26428499999999999 -y -9.9723799999999994 attribpromote1
opspareds "" attribpromote1
opparm -V 19.5.303 attribpromote1 inname ( Cd ) inclass ( point ) outclass ( vertex ) usepieceattrib ( off ) pieceattrib ( name ) method ( mean ) useoutname ( off ) outname ( "" ) deletein ( on )
chlock attribpromote1 -*
chautoscope attribpromote1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribpromote1
opexprlanguage -s hscript attribpromote1
opuserdata -n '___Version___' -v '19.5.303' attribpromote1

# Node comb1 (Sop/comb)
opadd -e -n comb comb1
oplocate -x -0.26428499999999999 -y -7.8138899999999998 comb1
opspareds "" comb1
opparm -V 19.5.303 comb1 group ( "" ) switcher ( 0 0 0 0 ) lift ( 0 ) preservenml ( on ) ovrnml ( off ) nmlname ( N ) clearall ( 0 ) op ( comb ) shape ( circle ) bitmap ( butterfly1.pic ) bmpchan ( alpha ) rad ( 0.24486295878887177 ) uvradius ( 0.10000000149011612 ) radiuspressure ( 1 ) usedepth ( off ) depth ( 0.10000000000000001 0.10000000000000001 ) brushangle ( 0 ) squash ( 1 ) opacity ( 1 ) opacitypresure ( 1 ) brushsplatter ( 0 ) papergrain ( 0 ) softedge ( 1 ) kernel ( elendt ) uptype ( fixed ) upvector ( 0 1 0 ) doreflect ( off ) dorotate ( off ) symaxis ( 0 0 1 ) symorig ( 0 0 0 ) symrot ( 3 ) symdist ( 0 ) douvreflect ( off ) symuvorig ( 0.5 0.5 ) symuvangle ( 90 ) projtype ( on ) useconnectivity ( on ) usenormals ( on ) realtime ( off ) dir ( 0.059942405670881271 0.99818921089172363 0.0050173262134194374 ) hitpos ( 2.3568682670593262 -0.15504646301269531 1.582639217376709 ) hitprim ( 1296 ) hituvw ( 0.56004494428634644 0.32452106475830078 0 ) hitpressure ( 1 ) hitpt ( -1 ) event ( begin )
chlock comb1 -*
chautoscope comb1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off comb1
opexprlanguage -s hscript comb1
opuserdata -n '___Version___' -v '19.5.303' comb1

# Node box1 (Sop/box)
opadd -e -n box box1
oplocate -x -5.3896658343759754 -y 1.93445 box1
opspareds "" box1
opparm -V 19.5.303 box1 type ( polymesh ) surftype ( quads ) consolidatepts ( on ) size ( 1 1 1 ) t ( 0 0 0 ) r ( 0 0 0 ) scale ( 1 ) divrate ( 2 2 2 ) orderrate ( 4 4 4 ) dodivs ( off ) divs ( 3 3 3 ) rebar ( off ) orientedbbox ( off ) vertexnormals ( off )
chlock box1 -*
chautoscope box1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off box1
opexprlanguage -s hscript box1
opuserdata -n '___Version___' -v '19.5.303' box1

# Node line1 (Sop/line)
opadd -e -n line line1
oplocate -x 6.2476167439259251 -y -1.4487753031357793 line1
opspareds "" line1
chblockbegin
chadd -t 0 0 line1 originx
chkey -t 0 -v 0 -m 0 -a 0 -A 0 -T a  -F '-0.5*ch("dist")' line1/originx
chblockend
opparm -V 19.5.303 line1 type ( poly ) origin ( originx 0 0 ) dir ( 1 0 0 ) dist ( 0.80000000000000004 ) points ( 2 ) order ( 4 )
chlock line1 -*
chautoscope line1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off line1
opexprlanguage -s hscript line1
opuserdata -n '___Version___' -v '19.5.303' line1

# Node sweep1 (Sop/sweep::2.0)
opadd -e -n sweep::2.0 sweep1
oplocate -x 4.7868167439259253 -y -5.0277534031357796 sweep1
opspareds "" sweep1
opparm sweep1  scaleramp ( 2 )
opparm -V 19.5.303 sweep1 curvegroup ( "" ) crosssectiongroup ( "" ) sepparm ( ) surface_folder ( 0 0 0 ) surfaceshape ( input ) surfacetype ( quads ) scale ( 1 ) cols ( 8 ) radius ( 0.10000000000000001 ) width ( 0.20000000000000001 ) reversecrosssections ( off ) stretcharoundturns ( on ) maxstretcharoundturns ( 10 ) endcaps_folder ( 0 ) endcaptype ( none ) capdivs ( 3 ) triangularpoles ( off ) capscale ( 1 ) caproundness ( 1 ) addendcapsgroup ( off ) endcapsgroup ( endcaps ) scale_folder ( 0 ) applyscale ( off ) scaleramp ( 2 ) rotation_folder ( 0 ) rOrd ( xyz ) applyroll ( on ) roll ( 0 ) fulltwists ( 0 ) incroll ( 0 ) rollper ( fulldistance ) rollattrib ( roll ) sepparmroll ( ) applyyaw ( off ) yaw ( 0 ) incyaw ( 0 ) yawper ( fulldistance ) yawattrib ( yaw ) sepparmyaw ( ) applypitch ( off ) pitch ( 0 ) incpitch ( 0 ) pitchper ( fulldistance ) pitchattrib ( pitch ) cross_sections_folder ( 0 ) copyorder ( each ) crosssectionattrib ( variant ) primtype ( auto ) unrollclosedrowcol ( off ) swaprowcol ( off ) closeifnocurveinput ( off ) up_folder ( 0 ) upvectortype ( normal ) upvectoratstart ( on ) useendupvector ( off ) upvectorattrib ( start_up ) endupvectorattrib ( end_up ) upvector ( 0 1 0 ) endupvector ( 0 1 0 ) tangents_folder ( 0 ) tangenttype ( avgdir ) continuousclosed ( on ) extrapolateendtangents ( off ) transformbyattribs ( on ) uv_folder ( 0 ) computeuvs ( off ) overrideexistinguvs ( off ) lengthweighteduvs ( on ) normalizeu ( on ) normalizev ( off ) flipu ( on ) uvscale_folder ( 0 ) uvscale ( 1 1 ) usemeshedgelengths ( on ) propscalepercurve ( on ) uvseams_folder ( 0 ) wrapu ( on ) wrapv ( on ) attributes_folder ( 0 ) input_folder ( 0 ) attribsfrombackbone ( '* ^P ^N ^up ^pscale ^scale ^orient ^rot ^pivot ^trans ^transform' ) attribsfromcrosssection ( * ) output_folder ( 0 ) addptrow ( off ) ptrowattrib ( ptrow ) addptcol ( off ) ptcolattrib ( ptcol ) addprimrow ( off ) primrowattrib ( primrow ) addprimcol ( off ) primcolattrib ( primcol ) addcrosssectionnum ( off ) crosssectionnumattrib ( crossnum ) addcurvenum ( off ) curvenumattrib ( curvenum ) scaleramp1pos ( 0 ) scaleramp1value ( 1 ) scaleramp1interp ( linear ) scaleramp2pos ( 1 ) scaleramp2value ( 1 ) scaleramp2interp ( linear )
chlock sweep1 -*
chautoscope sweep1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off sweep1
opexprlanguage -s hscript sweep1
opuserdata -n '___Version___' -v '19.5.303' sweep1

# Node resample1 (Sop/resample)
opadd -e -n resample resample1
oplocate -x 2.9021414753827237 -y -4.1948010741728066 resample1
opspareds "" resample1
opparm -V 19.5.303 resample1 group ( "" ) maintainprimorder ( off ) lod ( 1 ) edge ( off ) method ( dist ) measure ( arc ) dolength ( on ) length ( 0.10000000000000001 ) dosegs ( off ) segs ( 10 ) useattribs ( on ) allequal ( on ) last ( off ) randomshift ( off ) onlypoints ( off ) treatpolysas ( straight ) outputsubdpoly ( off ) doptdistattr ( off ) ptdistattr ( ptdist ) dotangentattr ( off ) tangentattr ( tangentu ) docurveuattr ( off ) curveuattr ( curveu ) docurvenumattr ( off ) curvenumattr ( curvenum )
chlock resample1 -*
chautoscope resample1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off resample1
opexprlanguage -s hscript resample1
opuserdata -n '___Version___' -v '19.5.303' resample1

# Node resample2 (Sop/resample)
opadd -e -n resample resample2
oplocate -x 6.2476167439259251 -y -2.5940434031357791 resample2
opspareds "" resample2
opparm -V 19.5.303 resample2 group ( "" ) maintainprimorder ( off ) lod ( 1 ) edge ( off ) method ( dist ) measure ( arc ) dolength ( on ) length ( 0.10000000000000001 ) dosegs ( off ) segs ( 10 ) useattribs ( on ) allequal ( on ) last ( off ) randomshift ( off ) onlypoints ( off ) treatpolysas ( straight ) outputsubdpoly ( off ) doptdistattr ( off ) ptdistattr ( ptdist ) dotangentattr ( off ) tangentattr ( tangentu ) docurveuattr ( off ) curveuattr ( curveu ) docurvenumattr ( off ) curvenumattr ( curvenum )
chlock resample2 -*
chautoscope resample2 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off resample2
opexprlanguage -s hscript resample2
opuserdata -n '___Version___' -v '19.5.303' resample2

# Node subdivide1 (Sop/subdivide)
opadd -e -n subdivide subdivide1
oplocate -x -10.1067 -y 4.7562199999999999 subdivide1
opspareds "" subdivide1
opparm -V 19.5.303 subdivide1 subdivide ( '@name=bg/scene_home_fh_day00101_bg04' ) creases ( "" ) algorithm ( osdcc ) iterations ( 8 ) overridecrease ( on ) creaseweight ( 100 ) outputcrease ( off ) outcreasegroup ( creases ) closeholes ( pull ) surroundpoly ( edges ) bias ( 1 ) updatenmls ( on ) smoothvertex ( on ) consisttopology ( off ) linearcreases ( off ) buildpolysoups ( off ) indepcurves ( off ) removeholes ( on ) vtxboundary ( corner ) fvarlinear ( corner1 ) creasemethod ( uniform ) trianglesubd ( smooth )
chlock subdivide1 -*
chautoscope subdivide1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off subdivide1
opexprlanguage -s hscript subdivide1
opuserdata -n '___Version___' -v '19.5.303' subdivide1

# Node attribwrangle1 (Sop/attribwrangle)
opadd -e -n attribwrangle attribwrangle1
oplocate -x -4.5976100000000004 -y -12.5503 attribwrangle1
opspareds "" attribwrangle1
opparm attribwrangle1  bindings ( 0 ) groupbindings ( 0 )
opparm attribwrangle1 folder0 ( 0 0 ) group ( "" ) grouptype ( points ) class ( point ) vex_numcount ( 10 ) vex_threadjobsize ( 1024 ) snippet ( '@Cd = set(0.5, 0.5, 0);\nint posprim;\nvector nearPUV;\nfloat maxdist = 10;\nfloat dist = xyzdist(1, @P, posprim, nearPUV, maxdist);\nif(dist<0.1)\n{\n    vector nearColor = primuv(1, "Cd", posprim, nearPUV);\n    @Cd = nearColor;\n}\n' ) exportlist ( * ) vex_strict ( off ) autobind ( on ) bindings ( 0 ) groupautobind ( on ) groupbindings ( 0 ) vex_cwdpath ( . ) vex_outputmask ( * ) vex_updatenmls ( off ) vex_matchattrib ( id ) vex_inplace ( off ) vex_selectiongroup ( "" ) vex_precision ( auto )
chlock attribwrangle1 -*
chautoscope attribwrangle1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribwrangle1
opexprlanguage -s hscript attribwrangle1
opuserdata -n '___Version___' -v '' attribwrangle1

# Node OUT_Flowmap (Sop/null)
opadd -e -n null OUT_Flowmap
oplocate -x -4.5946100000000003 -y -16.891500000000001 OUT_Flowmap
opspareds "" OUT_Flowmap
opparm -V 19.5.303 OUT_Flowmap copyinput ( on ) cacheinput ( off )
chlock OUT_Flowmap -*
chautoscope OUT_Flowmap -*
opset -d on -r on -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off OUT_Flowmap
opexprlanguage -s hscript OUT_Flowmap
opuserdata -n '___Version___' -v '19.5.303' OUT_Flowmap

# Node assemble1 (Sop/assemble)
opadd -e -n assemble assemble1
oplocate -x -4.5946100000000003 -y -15.785299999999999 assemble1
opspareds "" assemble1
opparm -V 2 assemble1 group ( "" ) inside_group ( inside ) outside_group ( piece ) keepattrib ( off ) pieceattrib ( __fracture_class__ ) newgroups ( off ) newname ( on ) connect ( on ) doCusp ( off ) pack_geo ( off ) createpath ( off ) path ( 'op:`opfullpath(\'.\')`' ) createpackedfragments ( on ) transfer_attributes ( "" ) transfer_groups ( "" ) pivot ( centroid ) viewportlod ( full )
chlock assemble1 -*
chautoscope assemble1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off assemble1
opexprlanguage -s hscript assemble1
opuserdata -n '___Version___' -v '2' assemble1
opuserdata -n '___toolcount___' -v '5' assemble1
opuserdata -n '___toolid___' -v 'dop_rbdfracturedobject' assemble1

# Node attribpromote2 (Sop/attribpromote)
opadd -e -n attribpromote attribpromote2
oplocate -x -4.5980600000000003 -y -13.6074 attribpromote2
opspareds "" attribpromote2
opparm -V 19.5.303 attribpromote2 inname ( Cd ) inclass ( point ) outclass ( vertex ) usepieceattrib ( off ) pieceattrib ( name ) method ( mean ) useoutname ( off ) outname ( "" ) deletein ( on )
chlock attribpromote2 -*
chautoscope attribpromote2 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribpromote2
opexprlanguage -s hscript attribpromote2
opuserdata -n '___Version___' -v '19.5.303' attribpromote2

# Node attribdelete1 (Sop/attribdelete)
opadd -e -n attribdelete attribdelete1
oplocate -x -4.5980600000000003 -y -14.651 attribdelete1
opspareds "" attribdelete1
opparm attribdelete1 usereference ( off ) negate ( on ) doptdel ( on ) ptdel ( "" ) dovtxdel ( on ) vtxdel ( 'N uv Cd' ) doprimdel ( on ) primdel ( "" ) dodtldel ( on ) dtldel ( "" ) updatevar ( on )
chlock attribdelete1 -*
chautoscope attribdelete1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribdelete1
opexprlanguage -s hscript attribdelete1
opuserdata -n '___Version___' -v '' attribdelete1

# Node attribwrangle2 (Sop/attribwrangle)
opadd -e -n attribwrangle attribwrangle2
oplocate -x 1.13913 -y 0.58603099999999997 attribwrangle2
opspareds "" attribwrangle2
opparm attribwrangle2  bindings ( 0 ) groupbindings ( 0 )
opparm attribwrangle2 folder0 ( 0 0 ) group ( "" ) grouptype ( guess ) class ( point ) vex_numcount ( 10 ) vex_threadjobsize ( 1024 ) snippet ( '@up = {0, 1, 0};' ) exportlist ( * ) vex_strict ( off ) autobind ( on ) bindings ( 0 ) groupautobind ( on ) groupbindings ( 0 ) vex_cwdpath ( . ) vex_outputmask ( * ) vex_updatenmls ( off ) vex_matchattrib ( id ) vex_inplace ( off ) vex_selectiongroup ( "" ) vex_precision ( auto )
chlock attribwrangle2 -*
chautoscope attribwrangle2 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribwrangle2
opexprlanguage -s hscript attribwrangle2
opuserdata -n '___Version___' -v '' attribwrangle2

# Node smooth1 (Sop/smooth::2.0)
opadd -e -n smooth::2.0 smooth1
oplocate -x -0.52210100000000004 -y -2.0524111700740963 smooth1
opspareds "" smooth1
opparm -V 19.5.303 smooth1 group ( "" ) contrainedboundary ( groupboundary ) constrainedpoints ( "" ) attributes ( P ) method ( uniform ) strength ( 10 ) filterquality ( 2 )
chlock smooth1 -*
chautoscope smooth1 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off smooth1
opexprlanguage -s hscript smooth1
opuserdata -n '___Version___' -v '19.5.303' smooth1

# Node transform2 (Sop/xform)
opadd -e -n xform transform2
oplocate -x -0.52210100000000004 -y -3.3769771138482141 transform2
opspareds "" transform2
opparm -V 19.5.303 transform2 group ( "" ) grouptype ( guess ) xOrd ( srt ) rOrd ( xyz ) t ( 0 0.01 0 ) r ( 0 0 0 ) s ( 1 1 1 ) shear ( 0 0 0 ) scale ( 1 ) parmgroup_pivotxform ( 0 ) p ( 0 0 0 ) pr ( 0 0 0 ) parmgroup_prexform ( 0 ) prexform_xOrd ( srt ) prexform_rOrd ( xyz ) prexform_t ( 0 0 0 ) prexform_r ( 0 0 0 ) prexform_s ( 1 1 1 ) prexform_shear ( 0 0 0 ) movecentroid ( 0 ) attribs ( * ) updatenmls ( off ) updateaffectednmls ( on ) vlength ( on ) invertxform ( off ) addattrib ( off ) outputattrib ( xform ) outputmerge ( post )
chlock transform2 -*
chautoscope transform2 -* +tx +ty +tz +rx +ry +rz +sx +sy +sz
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off transform2
opexprlanguage -s hscript transform2
opuserdata -n '___Version___' -v '19.5.303' transform2

# Node attribwrangle3 (Sop/attribwrangle)
opadd -e -n attribwrangle attribwrangle3
oplocate -x -7.6698195441769057 -y -12.5503 attribwrangle3
opspareds "" attribwrangle3
opparm attribwrangle3  bindings ( 0 ) groupbindings ( 0 )
opparm attribwrangle3 folder0 ( 0 0 ) group ( "" ) grouptype ( points ) class ( point ) vex_numcount ( 10 ) vex_threadjobsize ( 1024 ) snippet ( '@Cd = set(0, 0, 0);\nint posprim;\nvector nearPUV;\nfloat maxdist = 10;\nfloat dist = xyzdist(1, @P, posprim, nearPUV, maxdist);\nif(dist<0.1)\n{\n    vector nearColor = primuv(1, "Cd", posprim, nearPUV);\n    @Cd = set(1,1,1);\n}\n' ) exportlist ( * ) vex_strict ( off ) autobind ( on ) bindings ( 0 ) groupautobind ( on ) groupbindings ( 0 ) vex_cwdpath ( . ) vex_outputmask ( * ) vex_updatenmls ( off ) vex_matchattrib ( id ) vex_inplace ( off ) vex_selectiongroup ( "" ) vex_precision ( auto )
chlock attribwrangle3 -*
chautoscope attribwrangle3 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribwrangle3
opexprlanguage -s hscript attribwrangle3
opuserdata -n '___Version___' -v '' attribwrangle3

# Node OUT_RiverMask (Sop/null)
opadd -e -n null OUT_RiverMask
oplocate -x -7.6668195476432883 -y -16.891500000000001 OUT_RiverMask
opspareds "" OUT_RiverMask
opparm -V 19.5.303 OUT_RiverMask copyinput ( on ) cacheinput ( off )
chlock OUT_RiverMask -*
chautoscope OUT_RiverMask -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off OUT_RiverMask
opexprlanguage -s hscript OUT_RiverMask
opuserdata -n '___Version___' -v '19.5.303' OUT_RiverMask

# Node attribpromote3 (Sop/attribpromote)
opadd -e -n attribpromote attribpromote3
oplocate -x -7.6702695476432892 -y -13.6074 attribpromote3
opspareds "" attribpromote3
opparm -V 19.5.303 attribpromote3 inname ( Cd ) inclass ( point ) outclass ( vertex ) usepieceattrib ( off ) pieceattrib ( name ) method ( mean ) useoutname ( off ) outname ( "" ) deletein ( on )
chlock attribpromote3 -*
chautoscope attribpromote3 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribpromote3
opexprlanguage -s hscript attribpromote3
opuserdata -n '___Version___' -v '19.5.303' attribpromote3

# Node assemble2 (Sop/assemble)
opadd -e -n assemble assemble2
oplocate -x -7.6668195476432883 -y -15.785299999999999 assemble2
opspareds "" assemble2
opparm -V 2 assemble2 group ( "" ) inside_group ( inside ) outside_group ( piece ) keepattrib ( off ) pieceattrib ( __fracture_class__ ) newgroups ( off ) newname ( on ) connect ( on ) doCusp ( off ) pack_geo ( off ) createpath ( off ) path ( 'op:`opfullpath(\'.\')`' ) createpackedfragments ( on ) transfer_attributes ( "" ) transfer_groups ( "" ) pivot ( centroid ) viewportlod ( full )
chlock assemble2 -*
chautoscope assemble2 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off assemble2
opexprlanguage -s hscript assemble2
opuserdata -n '___Version___' -v '2' assemble2
opuserdata -n '___toolcount___' -v '5' assemble2
opuserdata -n '___toolid___' -v 'dop_rbdfracturedobject' assemble2

# Node attribdelete2 (Sop/attribdelete)
opadd -e -n attribdelete attribdelete2
oplocate -x -7.6702695476432892 -y -14.651 attribdelete2
opspareds "" attribdelete2
opparm attribdelete2 usereference ( off ) negate ( on ) doptdel ( on ) ptdel ( "" ) dovtxdel ( on ) vtxdel ( 'N uv Cd' ) doprimdel ( on ) primdel ( "" ) dodtldel ( on ) dtldel ( "" ) updatevar ( on )
chlock attribdelete2 -*
chautoscope attribdelete2 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribdelete2
opexprlanguage -s hscript attribdelete2
opuserdata -n '___Version___' -v '' attribdelete2

# Node attribwrangle4 (Sop/attribwrangle)
opadd -e -n attribwrangle attribwrangle4
oplocate -x -10.501957554219096 -y -12.5503 attribwrangle4
opspareds "" attribwrangle4
opparm attribwrangle4  bindings ( 0 ) groupbindings ( 0 )
opparm attribwrangle4 folder0 ( 0 0 ) group ( "" ) grouptype ( points ) class ( point ) vex_numcount ( 10 ) vex_threadjobsize ( 1024 ) snippet ( '@Cd = set(1, 1, 1);\nint posprim;\nvector nearPUV;\nfloat maxdist = 10;\nfloat dist = xyzdist(1, @P, posprim, nearPUV, maxdist);\nif(dist<1)\n{\n    @Cd = set(dist,dist,dist);\n}\n' ) exportlist ( * ) vex_strict ( off ) autobind ( on ) bindings ( 0 ) groupautobind ( on ) groupbindings ( 0 ) vex_cwdpath ( . ) vex_outputmask ( * ) vex_updatenmls ( off ) vex_matchattrib ( id ) vex_inplace ( off ) vex_selectiongroup ( "" ) vex_precision ( auto )
chlock attribwrangle4 -*
chautoscope attribwrangle4 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribwrangle4
opexprlanguage -s hscript attribwrangle4
opuserdata -n '___Version___' -v '' attribwrangle4

# Node attribdelete3 (Sop/attribdelete)
opadd -e -n attribdelete attribdelete3
oplocate -x -10.502407552756175 -y -14.651 attribdelete3
opspareds "" attribdelete3
opparm attribdelete3 usereference ( off ) negate ( on ) doptdel ( on ) ptdel ( "" ) dovtxdel ( on ) vtxdel ( 'N uv Cd' ) doprimdel ( on ) primdel ( "" ) dodtldel ( on ) dtldel ( "" ) updatevar ( on )
chlock attribdelete3 -*
chautoscope attribdelete3 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribdelete3
opexprlanguage -s hscript attribdelete3
opuserdata -n '___Version___' -v '' attribdelete3

# Node attribpromote4 (Sop/attribpromote)
opadd -e -n attribpromote attribpromote4
oplocate -x -10.502407552756175 -y -13.6074 attribpromote4
opspareds "" attribpromote4
opparm -V 19.5.303 attribpromote4 inname ( Cd ) inclass ( point ) outclass ( vertex ) usepieceattrib ( off ) pieceattrib ( name ) method ( mean ) useoutname ( off ) outname ( "" ) deletein ( on )
chlock attribpromote4 -*
chautoscope attribpromote4 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off attribpromote4
opexprlanguage -s hscript attribpromote4
opuserdata -n '___Version___' -v '19.5.303' attribpromote4

# Node assemble3 (Sop/assemble)
opadd -e -n assemble assemble3
oplocate -x -10.498957552756174 -y -15.785299999999999 assemble3
opspareds "" assemble3
opparm -V 2 assemble3 group ( "" ) inside_group ( inside ) outside_group ( piece ) keepattrib ( off ) pieceattrib ( __fracture_class__ ) newgroups ( off ) newname ( on ) connect ( on ) doCusp ( off ) pack_geo ( off ) createpath ( off ) path ( 'op:`opfullpath(\'.\')`' ) createpackedfragments ( on ) transfer_attributes ( "" ) transfer_groups ( "" ) pivot ( centroid ) viewportlod ( full )
chlock assemble3 -*
chautoscope assemble3 -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off assemble3
opexprlanguage -s hscript assemble3
opuserdata -n '___Version___' -v '2' assemble3
opuserdata -n '___toolcount___' -v '5' assemble3
opuserdata -n '___toolid___' -v 'dop_rbdfracturedobject' assemble3

# Node OUT_RiverMaskEx (Sop/null)
opadd -e -n null OUT_RiverMaskEx
oplocate -x -10.498957552756174 -y -16.891500000000001 OUT_RiverMaskEx
opspareds "" OUT_RiverMaskEx
opparm -V 19.5.303 OUT_RiverMaskEx copyinput ( on ) cacheinput ( off )
chlock OUT_RiverMaskEx -*
chautoscope OUT_RiverMaskEx -*
opset -d off -r off -h off -f off -y off -t off -l off -s off -u off -F on -c on -e on -b off OUT_RiverMaskEx
opexprlanguage -s hscript OUT_RiverMaskEx
opuserdata -n '___Version___' -v '19.5.303' OUT_RiverMaskEx
oporder -e bg flowmap1 flowmap_guide1 curve2 flowmap_to_color1 ray1 transform1 blast1 RiverMesh attribpromote1 comb1 box1 line1 sweep1 resample1 resample2 subdivide1 attribwrangle1 OUT_Flowmap assemble1 attribpromote2 attribdelete1 attribwrangle2 smooth1 transform2 attribwrangle3 OUT_RiverMask attribpromote3 assemble2 attribdelete2 attribwrangle4 attribdelete3 attribpromote4 assemble3 OUT_RiverMaskEx 
opcf ..
opcf ..
opcf out
oporder -e flowmap_ssrpg_flowmap flowmap_ssrpg_river_mask flowmap_ssrpg_river_mask_EX 
opcf ..

# Node ch (/ch)

# Node shop (/shop)

# Node img (/img)
opcf mat
opcf ..

# Node stage (/stage)

# Node tasks (/tasks)
oporder -e obj out ch shop img vex mat stage tasks 

opcf /
opcf obj
opcf bg
opwire -n sweep1 -0 flowmap1
opwire -n flowmap1 -0 flowmap_guide1
opwire -n transform2 -1 flowmap_guide1
opwire -n comb1 -0 flowmap_to_color1
opwire -n attribwrangle2 -0 ray1
opwire -n blast1 -1 ray1
opwire -n subdivide1 -0 transform1
opwire -n transform1 -0 blast1
opwire -n attribpromote1 -0 RiverMesh
opwire -n flowmap_to_color1 -0 attribpromote1
opwire -n flowmap_guide1 -0 comb1
opwire -n resample1 -0 sweep1
opwire -n resample2 -1 sweep1
opwire -n transform2 -0 resample1
opwire -n line1 -0 resample2
opwire -n bg -0 subdivide1
opwire -n transform1 -0 attribwrangle1
opwire -n RiverMesh -1 attribwrangle1
opwire -n assemble1 -0 OUT_Flowmap
opwire -n attribdelete1 -0 assemble1
opwire -n attribwrangle1 -0 attribpromote2
opwire -n attribpromote2 -0 attribdelete1
opwire -n curve2 -0 attribwrangle2
opwire -n ray1 -0 smooth1
opwire -n smooth1 -0 transform2
opwire -n transform1 -0 attribwrangle3
opwire -n RiverMesh -1 attribwrangle3
opwire -n assemble2 -0 OUT_RiverMask
opwire -n attribwrangle3 -0 attribpromote3
opwire -n attribdelete2 -0 assemble2
opwire -n attribpromote3 -0 attribdelete2
opwire -n transform1 -0 attribwrangle4
opwire -n resample1 -1 attribwrangle4
opwire -n attribpromote4 -0 attribdelete3
opwire -n attribwrangle4 -0 attribpromote4
opwire -n attribdelete3 -0 assemble3
opwire -n assemble3 -0 OUT_RiverMaskEx
opcf ..
opcf ..
opcf out
opcf ..
opcf mat
opcf ..
opcf $saved_path
