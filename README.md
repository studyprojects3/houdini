## 目录命名规则
如下目录会被ignore，这些目录中的内容都是可以再生成的。新的工程也应该尽量遵守如下的目录命名规则。

backup          houdini使用的backup目录
cache           用于缓存计算结果
cache/geo       用于缓存geo
out             用于缓存输出结果
